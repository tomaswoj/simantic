package com.tomaswoj.simantic.asm;

import org.junit.Assert;
import org.junit.Test;


public class AsmUtilsTests {

	@Test
	public void testUpperLower() {
		int address = 1044;
		Assert.assertTrue(Asm.getLower(address)==0x14);
		Assert.assertTrue(Asm.getUpper(address)==0x4);
		address = 48574;
		Assert.assertTrue(Asm.getLower(address)==0xbe);
		Assert.assertTrue(Asm.getUpper(address)==0xbd);
		
	}
}
