package com.tomaswoj.simantic.asm;

import org.junit.Test;
import org.junit.Assert;

public class AsmBankSwitching {

	public static final int MEMADDR=0x4000;
	
	Asm asm;
	
	@Test
	public void basicBankSwitchTest() {
		asm = Asm.getAsm64k();
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.cpu.acu==0);
		asm.ldx_imm(0xa);
		asm.stx(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xa);
		asm.switchBank(true, 1);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0x0);
		asm.switchBank(false, 0);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xa);
		asm.switchBank(true, 1);
		asm.inx();
		asm.stx(MEMADDR);
		asm.switchBank(true, 2);
		asm.inx();
		asm.stx(MEMADDR);
		asm.switchBank(true, 3);
		asm.inx();
		asm.stx(MEMADDR);
		asm.switchBank(true, 4);
		asm.inx();
		asm.stx(MEMADDR);
		asm.switchBank(false, 0);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xa);
		asm.switchBank(true, 1);
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xb);
		asm.switchBank(true, 2);
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xc);
		asm.switchBank(true, 3);
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xd);
		asm.switchBank(true, 4);
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xe);
		asm.switchBank(false, 0);
		asm.lda(MEMADDR);
		Assert.assertTrue(asm.mem.bytes[MEMADDR]==0xa);

	}
}
