package com.tomaswoj.simantic.utils;

import com.tomaswoj.simantic.hw.Memory;

import processing.core.PApplet;
import processing.core.PImage;

public class ImageUtils {

	public static void loadImageToMem(PApplet p, String filename, Memory memory, int offset, float colorScale) {
		PImage image;
		image = p.loadImage(filename);
		int width = image.width;
		int colVal;
		int height = image.height;
		int address = offset;
		for (int y=0;y<height;y++) {
			for (int x=0;x<width;x++) {
				colVal = (int) p.red(image.get(x, y));
				memory.bytes[address]=(int)(colorScale*colVal);
				address++;
			}
		}
	}
	
	public static void loadImageToMemPacked(PApplet p, String filename, Memory memory, int offset, float colorScale) {
		//pack each pixel into half byte
		PImage image;
		image = p.loadImage(filename);
		int width = image.width;
		int colVal;
		int colVal2;
		int height = image.height;
		int address = offset;
		for (int y=0;y<height;y++) {
			for (int x=0;x<width/2;x++) {
				colVal = (int) p.red(image.get(x*2, y));
				colVal = (int)(colorScale*colVal);
				colVal = colVal << 4;
				colVal2 = (int) p.red(image.get(x*2+1, y));
				colVal2 = (int)(colorScale*colVal2);
				colVal+=colVal2;
				//memory.bytes[address]=(int)(colorScale*colVal);
				memory.bytes[address]=colVal;
				address++;
			}
		}		
	}
}
