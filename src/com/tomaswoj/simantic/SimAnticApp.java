package com.tomaswoj.simantic;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.fx.Firestarter256;
import com.tomaswoj.simantic.fx.FxList;
import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.fx.FxVoxels;
import com.tomaswoj.simantic.fx.Sillyvitro256;
import com.tomaswoj.simantic.fx.TestFx;
import com.tomaswoj.simantic.fx.TestFxGtia9;
import com.tomaswoj.simantic.fx.TestFxHires;
import com.tomaswoj.simantic.fx.TestFxText;
import com.tomaswoj.simantic.fx.TestFxTextColor;
import com.tomaswoj.simantic.ui.SimAnticRootControllerCtx;
import com.tomaswoj.simantic.ui.SimAnticUIUtils;
import com.tomaswoj.simantic.ui.FxColorsRenderer;
import com.tomaswoj.simantic.ui.FxHelperViewRenderer;
import com.tomaswoj.simantic.ui.FxMemoryMapRenderer;
import com.tomaswoj.simantic.ui.FxProfilerRenderer;
import com.tomaswoj.simantic.ui.HelperRenderer;
import com.tomaswoj.simantic.ui.MemoryDisplay;
import com.tomaswoj.simantic.ui.MemoryViewController;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import processing.core.PApplet;

public class SimAnticApp extends Application {

	private static SimAnticApp app;
	private static SimAntic simAntic;
	private static FxList fxList;
	private static PApplet renderer;
	private static Logger logger;
	private static VBox rootLayout;
	Scene scene;
	private static Stage primaryStage;
	private static MemoryDisplay memDisplay;
	
	// helper views
	private static FxColorsRenderer colorRenderer;
	private static FxMemoryMapRenderer mmapRenderer;
	private static FxProfilerRenderer profilerRenderer;
	private static FxHelperViewRenderer helperviewRenderer;
	
	
	@FXML
	ComboBox fxCombo;

	@FXML
	Button fxInitBtn;

	@FXML
	CheckBox hexChk; 
	@FXML
	TextField offsetField;
	@FXML
	Button pauseBtn;
	@FXML
	Button playBtn;
	@FXML
	Button stepBtn;
	@FXML
	TextArea memoryArea;
	@FXML
	ComboBox<String> offsetBox;
	
	@FXML
	ComboBox<String> pixCombo;
	@FXML
	CheckBox palChk;
	@FXML
	CheckBox colChk;
	@FXML
	CheckBox statusChk;
	@FXML
	CheckBox helperChk;
	
	@FXML
	CheckBox mmapChk;
	
	@FXML
	CheckBox profilerChk;
	
	@FXML	
	Button peekBtn;
	@FXML
	Button pokeBtn;
	@FXML
	TextField addressField;
	@FXML
	TextField valueField;
		
	//show as bitmap
	@FXML
	TextField widthField;
	@FXML
	TextField heightField;
	@FXML
	TextField maxField;
	@FXML
	Button bitmapBtn;
	
	@FXML
	Button codeBtn;
	
	@FXML
	TextField filenameField;
	@FXML
	TextField startField;
	@FXML
	TextField endField;
	@FXML
	TextField sizeField;
	@FXML
	Button loadBtn;
	@FXML
	Button saveBtn;
	@FXML
	Button bankBtn;
	@FXML
	ComboBox bankCombo;
	
	
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage arg0) throws Exception {		
		  logger = LoggerFactory.getLogger(SimAnticApp.class);
		  logger.info("Initializing ProseSim");
		  primaryStage=arg0;
		  initRootLayout();
	}

	@Override
	  public void init() {		
		renderer = new PApplet();
		simAntic = new SimAntic();		
		renderer.runSketch(new String[] {""}, simAntic);
		initBasicFx(); // we need PApplet here!
		simAntic.setFxSim(fxList.getFx(fxList.getCurrentFx()));
		//offsetBox.getItems().addAll("test1", "test2", "test3");		
	}
	
	@FXML
	private void initialize() {
		refreshOffsetsCombo();
		refreshFxCombo();
		
		pixCombo.getItems().addAll("1x1", "2x2", "3x3", "4x4");
		pixCombo.getSelectionModel().select(0);
		bankCombo.getItems().addAll("0 - base RAM", "0 - 130XE", "1 - 130XE", "2 - 130XE", "3 - 130XE");
		bankCombo.getSelectionModel().select(0);
	}
	
	public void refreshFxCombo() {
		ObservableList<String> options = FXCollections.observableArrayList(fxList.getFxNames());
		if (options.size()>0) {
				if (fxCombo!=null) {					
					fxCombo.setItems(options);
					fxCombo.getSelectionModel().select(0);
				}
				else { System.out.println("fx box is null!");}
		}

	}
	
	public void refreshOffsetsCombo() {
		ObservableList<String> options = FXCollections.observableArrayList(fxList.getFx(fxList.getCurrentFx()).getOffsets());
		if (options.size()>0) {
				if (offsetBox!=null) {					
					offsetBox.setItems(options);
					offsetBox.getSelectionModel().select(0);
				}
				else { System.out.println("offsetbox is null!");}
		}
	}
	
	
	public void initBasicFx() {
		fxList = new FxList();
		// BASIC textmode, monochrome
		FxSim testFxSim = new TestFx();
		memDisplay = new MemoryDisplay();
		memDisplay.setMem(testFxSim.getMemory());		
		//testFxSim.playWhole();
		testFxSim.playCredits(220);
		fxList.addFx(testFxSim);		
		//GTIA9 with image
		FxSim testFxGtia9 = new TestFxGtia9();
		testFxGtia9.playCredits(10);
		fxList.addFx(testFxGtia9);
		//basic monochrome hires
		FxSim testFxHires = new TestFxHires();
		testFxHires.playCredits(10);
		fxList.addFx(testFxHires);
		FxSim testFxText = new TestFxText();
		testFxText.playCredits(10);
		fxList.addFx(testFxText);		
		FxSim testFxTextColor = new TestFxTextColor();
		testFxTextColor.playCredits(10);
		fxList.addFx(testFxTextColor);
		//FxSim firestarter = new Firestarter256();
		//firestarter.playCredits(10);
		//fxList.addFx(firestarter);
		FxSim sillyvitro = new Sillyvitro256();
		sillyvitro.playCredits(100);
		fxList.addFx(sillyvitro);
		FxSim voxels = new FxVoxels();
		voxels.playCredits(100);
		fxList.addFx(voxels);

	}
	
	public void reselectFx() {
		memDisplay = new MemoryDisplay();
		//reattach memory display
		memDisplay.setMem(fxList.getFx(fxList.getCurrentFx()).getMemory());
		//reattach renderer
		simAntic.setFxSim(fxList.getFx(fxList.getCurrentFx()));
		refreshOffsetsCombo();
		
		//re-attach helper views, if visible
		if (profilerRenderer!=null && profilerChk.isSelected()) {				
			profilerRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
		}
		if (mmapRenderer!=null) {
			mmapRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
		}
		if (helperviewRenderer!=null) {
			helperviewRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
		}
		if (colorRenderer!=null) {
			colorRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
		}
	}
	
	@FXML
	public void initSelectedFx() {
		//release the old semaphore or we may end up in a crash...
		fxList.getFx(fxList.getCurrentFx()).getSemaphore().release();
		fxList.setCurrentFx(fxCombo.getSelectionModel().getSelectedIndex());		
		reselectFx();
	}
		
	public void initRootLayout() {
		  try {
			  FXMLLoader loader = new FXMLLoader();
			  loader.setLocation(SimAnticApp.class.getResource("SimAnticUI.fxml"));
			  
			  rootLayout = (VBox) loader.load();
			  scene = new Scene(rootLayout);
			  rootLayout.applyCss();
			  primaryStage.setScene(scene);
			  primaryStage.setTitle("SimAntic v0.1");
			  primaryStage.show();
			  
		  } catch (Exception e) {
			  logger.error("Cannot initialize FXMLLoader");
			  e.printStackTrace();
		  }
		  		  
	  }

	@FXML
	public void clickedPauseBtn() {
		fxList.getFx(fxList.getCurrentFx()).playCredits(0);
	}

	@FXML
	public void clickedPlayBtn() {
		 //refreshOffsetsCombo();
		fxList.getFx(fxList.getCurrentFx()).playWhole();
	}

	@FXML
	public void clickedStepBtn() {
		fxList.getFx(fxList.getCurrentFx()).playStep();
	}
	
	@FXML
	public void clickedShowMemory() {
		int offset;
		// parse the offset field
		String offsetText = offsetField.getText();
		memDisplay.setOffset(addressParser(offsetText));
		memoryArea.setText(memDisplay.getFormattedData());
	}

	@FXML
	public void clickedShowCode() {
		int offset;
		// parse the offset field
		String offsetText = offsetField.getText();
		memDisplay.setOffset(addressParser(offsetText));
		memoryArea.setText(memDisplay.getPageAsCode());
	}

	
	@FXML
	public void clickedPeek() {
		if (addressField.getText()!=null && addressField.getText().length()>0) {
			int value =fxList.getFx(fxList.getCurrentFx()).getMemory().bytes[addressParser(addressField.getText())];
			valueField.setText("0x"+String.format("%02X", value));
		}
	}
	
	@FXML
	public void clickedPoke() {
		if (addressField.getText()!=null && addressField.getText().length()>0) {
			int address = addressParser(addressField.getText());
			int value = addressParser(valueField.getText());
			fxList.getFx(fxList.getCurrentFx()).getMemory().bytes[address]=value;
			System.out.println("POKEd:"+address+" with value:"+value);
		}
	}

	
	public int addressParser(String input) {
		int address;
		if (input==null || input.length()==0) return -1;
		if (input.contains("0x")) address = Integer.parseInt(input.substring(2),16);
		else address = Integer.parseInt(input);
		return address;
	}
	
	@FXML
	public void offsetSelected() {
		String selectedOffset = offsetBox.getSelectionModel().getSelectedItem();
		if (offsetField!=null && selectedOffset!=null) {
			offsetField.setText(selectedOffset.split(":")[1]);
		}
		
	}

	@FXML
	public void pixSelected() {
		int selectedPix = pixCombo.getSelectionModel().getSelectedIndex();
		fxList.getFx(fxList.getCurrentFx()).getAntic().setPixMul(selectedPix+1);
		simAntic.resizeView(320*(selectedPix+1)+20, 200*(selectedPix+1)+20);
		System.out.println("PixMul selected:"+selectedPix);
	}
	
	@FXML
	public void paletteSwitch() {
		if (palChk.isSelected()) {
			simAntic.setDrawPalette(true);
		}
		else simAntic.setDrawPalette(false);
	}
	

	@FXML
	public void statusSwitch() {
		if (statusChk.isSelected()) {
			fxList.getFx(fxList.getCurrentFx()).setShowStatus(true);
		}
		else fxList.getFx(fxList.getCurrentFx()).setShowStatus(false);
	}
	
	@FXML
	public void mmapSwitch() {
		if (mmapChk.isSelected()) {
			if (mmapRenderer==null) {				
				mmapRenderer = new FxMemoryMapRenderer();							
				mmapRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
				simAntic.runSketch(new String[] {"Memory Map"}, mmapRenderer);
				SimAnticUIUtils.removeExitEvent(mmapRenderer.getSurface());
			}
		}
		else {
			if (mmapRenderer!=null) {
				  mmapRenderer.stop();
				  mmapRenderer.frame.dispose();
				  mmapRenderer.getSurface().setVisible(false);			  			  			  
				  ((com.jogamp.newt.opengl.GLWindow) mmapRenderer.getSurface().getNative()).destroy();
				  mmapRenderer=null;
			}
		}
	}
	
	
	@FXML
	public void profilerSwitch() {
		if (profilerChk.isSelected()) {
			if (profilerRenderer==null) {				
				profilerRenderer = new FxProfilerRenderer();							
				profilerRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
				simAntic.runSketch(new String[] {"Profiler"}, profilerRenderer);
				SimAnticUIUtils.removeExitEvent(profilerRenderer.getSurface());
			}
		}
		else {
			if (profilerRenderer!=null) {
				  profilerRenderer.stop();
				  profilerRenderer.frame.dispose();
				  profilerRenderer.getSurface().setVisible(false);			  			  			  
				  ((com.jogamp.newt.opengl.GLWindow) profilerRenderer.getSurface().getNative()).destroy();
				  profilerRenderer=null;
			}
		}
	}
	
	@FXML
	public void colorsSwitch() {
		if (colChk.isSelected()) {
			if (colorRenderer==null) {				
				colorRenderer = new FxColorsRenderer();							
				colorRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
				simAntic.runSketch(new String[] {"Colors"}, colorRenderer);
				SimAnticUIUtils.removeExitEvent(colorRenderer.getSurface());
			}
		}
		else {
			if (colorRenderer!=null) {
				  colorRenderer.stop();
				  colorRenderer.frame.dispose();
				  colorRenderer.getSurface().setVisible(false);			  			  			  
				  ((com.jogamp.newt.opengl.GLWindow) colorRenderer.getSurface().getNative()).destroy();
				  colorRenderer=null;
			}
		}
	}

		
	@FXML
	public void helperSwitch() {
		if (helperChk.isSelected()) {
			if (helperviewRenderer==null) {				
				helperviewRenderer = new FxHelperViewRenderer();							
				helperviewRenderer.attachFx(fxList.getFx(fxList.getCurrentFx()));
				simAntic.runSketch(new String[] {"Helper View"}, helperviewRenderer);
				SimAnticUIUtils.removeExitEvent(helperviewRenderer.getSurface());
			}
		}
		else {
			if (helperviewRenderer!=null) {
				  helperviewRenderer.stop();
				  helperviewRenderer.frame.dispose();
				  helperviewRenderer.getSurface().setVisible(false);			  			  			  
				  ((com.jogamp.newt.opengl.GLWindow) helperviewRenderer.getSurface().getNative()).destroy();
				  helperviewRenderer=null;
			}
		}	}

	@FXML
	public void saveClicked() {
		String filename = filenameField.getText();
		int start = addressParser(startField.getText());
		int end = addressParser(endField.getText());
		int size = addressParser(sizeField.getText());
		
		if (filename.length()>0) {
			if (start>0 && (size>0 || end>0)) {
				//if size is provided it gets priority
				if (size>0) {
					fxList.getFx(fxList.getCurrentFx()).getMemory().dumpMemoryToFile(filename, start, size);
				}
				else
				{
					fxList.getFx(fxList.getCurrentFx()).getMemory().dumpMemoryToFile(filename, start, end-start);
				}
			}
		}
	}
	
	@FXML
	public void loadClicked() {
		String filename = filenameField.getText();
		int start = addressParser(startField.getText());
		int end = addressParser(endField.getText());
		int size = addressParser(sizeField.getText());
		
		if (filename.length()>0) {
			if (start>0 && (size>0 || end>0)) {
				//if size is provided it gets priority
				if (size>0) {
					fxList.getFx(fxList.getCurrentFx()).getMemory().loadDatToMemory(filename, start, size);;
				}
				else
				{
					fxList.getFx(fxList.getCurrentFx()).getMemory().loadDatToMemory(filename, start, end-start);
				}
			}
		}
		
	}

	@FXML
	public void showBitmap() {
		if (offsetField.getText().length()>0) {
			SimAnticRootControllerCtx ctx = SimAnticUIUtils.ctxLoader(SimAnticApp.class.getResource("MemoryView.fxml"));
			MemoryViewController controller = (MemoryViewController) ctx.getController();
		
			controller.setupViewer(addressParser(offsetField.getText()), 
					fxList.getFx(fxList.getCurrentFx()).getMemory(), 
					Integer.parseInt(widthField.getText()),
					Integer.parseInt(heightField.getText()), 
					Integer.parseInt(maxField.getText()));		
			SimAnticUIUtils.openUIWindow(ctx, "Memory as bitmap:");
			controller.updateCanvas();
			controller.handleStageClose(ctx.getStage());
		}
		else {
			System.out.println("Some empty fields for show bitmap:");
			System.out.println("Offset:"+offsetField.getText());
		}
	}
	
	@FXML
	public void switchBank() {
		//get selection from bank selection
		int selectedBankIndex = bankCombo.getSelectionModel().getSelectedIndex();
		switch (selectedBankIndex) {
			case 0: //base RAM
				fxList.getFx(fxList.getCurrentFx()).getAsm().switchBank(false, selectedBankIndex);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
				fxList.getFx(fxList.getCurrentFx()).getAsm().switchBank(true, selectedBankIndex);
				break;
			default:
				fxList.getFx(fxList.getCurrentFx()).getAsm().switchBank(false, 0);
		}
		System.out.println("Bank switched to:"+selectedBankIndex);
	}
	
}
