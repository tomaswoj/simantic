package com.tomaswoj.simantic.ui;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.fx.FxSimAbs;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;

import processing.core.PApplet;
import processing.core.PSurface;

public class FxHelperViewRenderer extends PApplet implements HelperRenderer {
	
	FxSim fx;
	private int prefWidth;
	private int prefHeight;

	@Override
	public String getHelperRendererName() {
		// TODO Auto-generated method stub
		return "Fx Helper View";
	}

	@Override
	public int getHelperWidth() {		
		return 320;
	}

	@Override
	public int getHelperHeight() {	
		return 320;
	}
	
	public void settings(){
		size(prefWidth, prefHeight, P3D);
		noSmooth();
	    }

            
        
	public void setup() {
	 frame.setResizable(true);
	 frame.setTitle(getHelperRendererName());	 
	 background(0);
	 }      

	
	public void draw() {		
	  background(0);
	  
	  if (fx!=null) 
	  {		
		  	try {
				fx.getSemaphore().acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  	fx.drawHelperView(this);
			fx.getSemaphore().release();
	  
		}
	}

	@Override
	public PSurface getHelperSurface() {		
		return this.getSurface();
	}
	
	public void attachFx(FxSim fx) {
		this.fx = fx;
		prefWidth = fx.getHelperWidth();
		prefHeight = fx.getHelperHeight();	
		if (frame!=null) frame.setSize(prefWidth, prefHeight);
	}
		

}
