package com.tomaswoj.simantic.ui;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.fx.FxSimAbs;
import com.tomaswoj.simantic.hw.AnticPalette;
import com.tomaswoj.simantic.hw.ColorRGB;
import com.tomaswoj.simantic.hw.GtiaRGB;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;

import processing.core.PApplet;
import processing.core.PSurface;

public class FxColorsRenderer extends PApplet implements HelperRenderer {
	
	FxSim fx;

	@Override
	public String getHelperRendererName() {
		// TODO Auto-generated method stub
		return "Atari Palette and FX Colors";
	}

	@Override
	public int getHelperWidth() {		
		return 320;
	}

	@Override
	public int getHelperHeight() {	
		return 450;
	}
	
	public void settings(){
		size(getHelperWidth(), getHelperHeight(), P3D);
		noSmooth();
	    }

            
        
	public void setup() {	 
	 frame.setTitle(getHelperRendererName());	 
	 background(0);
	 }      

	
	public void draw() {
	  background(0);
	  	  
	  if (fx!=null) 
	  {
		try {
			fx.getSemaphore().acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		drawColRegs(10,10);
		drawActualPalette(10,82);
		drawPalette(0,102);
		fx.getSemaphore().release();
	  }
	}

	public void drawPalette(int offsetX, int offsetY) {
		//get palette from AnticPalette (default for now)
		ColorRGB[] colors = AnticPalette.getPalette();
		int posX = offsetX+5;
		int posY = offsetY;
		int paletteOffset=0;
		noStroke();	
		// top text row
		textSize(8);
		fill(255);
		for (int i=0;i<16;i++) {
			text(String.format("%01X", i), posX+i*12+12, posY);		
		}
		for (int i=0;i<16;i++) {
			text(String.format("%01X", i), posX, posY+i*12+12);		
		}
		posY+=3;
		posX+=12;
		for (int i=0;i<16;i++) {
			for (int j=0;j<16;j++) {
				fill(colors[paletteOffset].red, colors[paletteOffset].green, colors[paletteOffset].blue);
				rect(posX+j*12, posY+i*12, 9,9);
				paletteOffset++;
			}
		}
	}

	public void drawColRegs(int offsetX, int offsetY) {
		textSize(8);
		noStroke();		
		ColorRGB col0 = GtiaRGB.gtia2RGB(fx.getMemory().bytes[MMap.COLOR0]);
		ColorRGB col1 = GtiaRGB.gtia2RGB(fx.getMemory().bytes[MMap.COLOR1]);
		ColorRGB col2 = GtiaRGB.gtia2RGB(fx.getMemory().bytes[MMap.COLOR2]);
		ColorRGB col3 = GtiaRGB.gtia2RGB(fx.getMemory().bytes[MMap.COLOR3]);
		ColorRGB col4 = GtiaRGB.gtia2RGB(fx.getMemory().bytes[MMap.COLOR4]);
		fill(255);
		text("Color registers:",offsetX, offsetY+6);
		
		text("COL0: $"+String.format("%02X", fx.getMemory().bytes[MMap.COLOR0]), offsetX, offsetY+18);
		text("COL1: $"+String.format("%02X", fx.getMemory().bytes[MMap.COLOR1]), offsetX, offsetY+30);
		text("COL2: $"+String.format("%02X", fx.getMemory().bytes[MMap.COLOR2]), offsetX, offsetY+42);
		text("COL3: $"+String.format("%02X", fx.getMemory().bytes[MMap.COLOR3]), offsetX, offsetY+54);
		text("COL4: $"+String.format("%02X", fx.getMemory().bytes[MMap.COLOR4]), offsetX, offsetY+66);
		fill(col0.red, col0.green, col0.blue);
		rect(offsetX+50, offsetY+12, 9, 9);
		fill(col1.red, col1.green, col1.blue);
		rect(offsetX+50, offsetY+24, 9, 9);
		fill(col2.red, col2.green, col2.blue);
		rect(offsetX+50, offsetY+36, 9, 9);
		fill(col3.red, col3.green, col3.blue);
		rect(offsetX+50, offsetY+48, 9, 9);
		fill(col4.red, col4.green, col4.blue);
		rect(offsetX+50, offsetY+60, 9, 9);
		
	}
	
	public void drawActualPalette (int offsetX, int offsetY) {
		if (fx.getAntic().coltable!=null)
		for (int i=0;i<16;i++) {
			if (fx.getAntic()!=null) {
			fill(fx.getAntic().coltable[i].red,
				 fx.getAntic().coltable[i].green,
				 fx.getAntic().coltable[i].blue);
			}
			rect(offsetX+12*i, offsetY, 9,9);
		}
	}

	@Override
	public PSurface getHelperSurface() {		
		return this.getSurface();
	}
	
	public void attachFx(FxSim fx) {
		this.fx = fx;
	}
		

}
