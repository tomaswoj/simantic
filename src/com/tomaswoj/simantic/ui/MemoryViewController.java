package com.tomaswoj.simantic.ui;

import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.hw.Memory;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import processing.core.PApplet;

public class MemoryViewController {

	private Logger logger;
	private static final int BUFFERSIZE=300;	
	private int min;
	private Thread tracingThread;
	private Stage stage;
	private Task task;
	
	private int offset;
	private Memory memory;
	private int width;
	private int height;
	private int max;
	

	
	@FXML
	Canvas tracerCanvas;
			
		
	public void setupViewer(int offset, Memory memory, int width, int height, int max) {
		this.offset = offset;
		this.memory=memory;
		this.width=width;
		this.height=height;
		this.max=max;		
	}
	
	public void handleStageClose(Stage myStage) {		
		this.stage=myStage;
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			 @Override
		        public void handle(final WindowEvent event) {
				 //logger.info("Stage close...");
				 //task.cancel();
			 }			
		});
	}
	
	
	public void startTracing() {
		/*
		    tracingThread = Thread.currentThread();
			updated.getAndSet(true);
			task = new Task<Integer>() {
//			//Thread someBackgroundThread = new Thread(() -> {
			@Override public Integer call() {
				while (!isCancelled()) {
					try {
						Thread.sleep(1000/60);		    		
					} catch (Exception e) {};
					if (updated.getAndSet(false)) {
						updateCanvas();
					}
				}
				return 0;
			}
			};
			Thread th = new Thread(task);
	         th.setDaemon(true);
	         th.start();
	         */	     	
	}


	public void updateCanvas() {
        GraphicsContext gc = tracerCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, tracerCanvas.getWidth(), tracerCanvas.getHeight());
        
        for (int y=0;y<height;y++) {
        	for (int x=0;x<width;x++) {   
        		int greyVal = memory.bytes[offset+y*width+x];
        		greyVal = 255*greyVal/max;
        		if (greyVal>max) 
        			gc.setFill(Color.RED);
        		else
            		gc.setFill(Color.rgb(greyVal, greyVal, greyVal));        	
        		gc.fillRect(10+x*5, 25+y*5, 5, 5);
        	}
        }
        
        
        gc.setStroke(Color.BLACK);
        gc.setFill(Color.BLACK);               
        gc.fillText("Memory at:"+offset+" ["+width+","+height+"], max:"+max, 10,20);


        /*
		    Platform.runLater(() -> {
		    	updateVals();
		        updated.set(true);
		        // repaint canvas...
		        GraphicsContext gc = tracerCanvas.getGraphicsContext2D();
		        gc.clearRect(0, 0, tracerCanvas.getWidth(), tracerCanvas.getHeight());		        
		        //logger.info("tracer updating running");
		        gc.setStroke(Color.GRAY);
		        gc.setFill(Color.GRAY);
		        gc.setLineWidth(1);
		        gc.fillRect(20,100, 300, 1);
		        gc.fillText("0", 2,100);
		        gc.fillRect(20,50, 300, 1);
		        gc.fillText("50", 2,55);
		        gc.fillRect(20,150, 300, 1);
		        gc.fillText("-50", 2,155);
		        for (int i=0;i<this.BUFFERSIZE;i++) {
			        gc.getPixelWriter().setColor(i+20, 100-(int)valueHistory[i], Color.BLACK);
		        }
		});
		*/
	}
	
		
	@FXML
	public void initialize() {
		//logger = LoggerFactory.getLogger(MemoryViewController.class.getName());
	}

}
