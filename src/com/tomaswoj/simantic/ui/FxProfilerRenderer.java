package com.tomaswoj.simantic.ui;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.fx.FxSimAbs;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;

import processing.core.PApplet;
import processing.core.PSurface;

public class FxProfilerRenderer extends PApplet implements HelperRenderer {
	
	FxSim fx;

	@Override
	public String getHelperRendererName() {
		// TODO Auto-generated method stub
		return "Fx Profiler";
	}

	@Override
	public int getHelperWidth() {		
		return 320;
	}

	@Override
	public int getHelperHeight() {	
		return 400;
	}
	
	public void settings(){
		size(getHelperWidth(), getHelperHeight(), P3D);
		noSmooth();
	    }

            
        
	public void setup() {	 
	 frame.setTitle(getHelperRendererName());	 
	 background(0);
	 }      

	
	public void draw() {
	  background(0);
	  
	  if (fx!=null) 
	  {		
		  	try {
				fx.getSemaphore().acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//show section counts
			int yOffset=10;
			int xOffset=10;
			text("Cycles counts:", xOffset, 12);
			HashMap<String,Integer> counts = fx.getAsm().getSectionCounts();
			if (counts!=null && counts.size()>0)
			for (String section:counts.keySet()) {
				text(section, xOffset, 14+yOffset);
				//draw a bar
				rect(xOffset+60, 14+yOffset-10, (counts.get(section)/1000), 10);
				text((counts.get(section)/1000)+"K", xOffset+60+5+(counts.get(section)/1000), 14+yOffset);
				yOffset+=12;
			}
			else {
				text("No profiler sections defined...", xOffset, 14+yOffset);
			}
			fx.getSemaphore().release();
	  
		}
	}

	@Override
	public PSurface getHelperSurface() {		
		return this.getSurface();
	}
	
	public void attachFx(FxSim fx) {
		this.fx = fx;
	}
		

}
