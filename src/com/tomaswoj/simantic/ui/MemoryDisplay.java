package com.tomaswoj.simantic.ui;

import com.tomaswoj.simantic.asm.Asm;
import com.tomaswoj.simantic.hw.Memory;

public class MemoryDisplay {

	private boolean showHex;
	private Memory mem;
	private int offset;
	
	public boolean isShowHex() {
		return showHex;
	}
	public void setShowHex(boolean showHex) {
		this.showHex = showHex;
	}
	public Memory getMem() {
		return mem;
	}
	public void setMem(Memory mem) {
		this.mem = mem;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	} 
	
	public String getFormattedData() {		
		return getFormattedData(256); //return a single page
	}
	
	public String getFormattedData(int size) {
		if (mem==null) {
			System.out.println("Memory display: mem not initialized!");
			return "";
		}
		String result= "";
		int lineCount=0;
		for (int i=0;i<size;i++) {
			if (lineCount==0) {
				result+=String.format("%02X", offset+i)+" | ";
			}
			result+=String.format("%02X", mem.bytes[offset+i]);
			result+=" ";
			lineCount++;
			if (lineCount==16) {
				lineCount=0;
				result+="\n";
			}
		}		
		return result;
	}
	
	public String getPageAsCode() {
		if (mem==null) {
			System.out.println("Memory display: mem not initialized!");
			return "";
		}
		String result= "";
		int caddr = offset;
		boolean codeParseFail=false;
		while (caddr<offset+256) {
			if (!codeParseFail) {
				result+=String.format("%02X", caddr)+" | ";
				if (Asm.getMnemonicSize(mem.bytes[caddr])>0) {
					//code recognized
					result+=Asm.getMnemonic(mem.bytes[caddr]);
					if(Asm.getMnemonicSize(mem.bytes[caddr])>2) {
						result+=" "+String.format("%02X", mem.bytes[caddr+1])+" "+ String.format("%02X", mem.bytes[caddr+2])+"\n";
						caddr+=3;
					}
					else if(Asm.getMnemonicSize(mem.bytes[caddr])>1){
						result+=" "+String.format("%02X", mem.bytes[caddr+1])+"\n";
						caddr+=2;
					}
					else {
						result+="\n";
						caddr++;
					}
				}
				else {
					result+="("+mem.bytes[caddr]+")\n";
					codeParseFail=true;
					caddr++;
				}
			}
			else {
				//just print down the binary 
				result+=String.format("%02X", caddr)+" | "+String.format("%02X", mem.bytes[caddr])+"\n";
				caddr++;
			}
		}
		return result;
	}
	
}
