package com.tomaswoj.simantic.ui;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.LoggerFactory;

import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;

import processing.core.PApplet;
import processing.core.PSurface;

public class FxMemoryMapRenderer extends PApplet implements HelperRenderer {
	
	FxSim fx;

	@Override
	public String getHelperRendererName() {
		// TODO Auto-generated method stub
		return "Fx Memory Map";
	}

	@Override
	public int getHelperWidth() {		
		return 320;
	}

	@Override
	public int getHelperHeight() {	
		return 400;
	}
	
	public void settings(){
		size(getHelperWidth(), getHelperHeight(), P3D);
		noSmooth();
	    }

            
        
	public void setup() {	 
	 frame.setTitle(getHelperRendererName());	 
	 background(0);
	 }      

	
	public void draw() {
	  background(0);	  
	  
	  if (fx!=null) 
	  {
		  	try {
				fx.getSemaphore().acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//show memory map (usage)
			stroke(255);
			int xOffset=10;
			int yOffset=10;			
			int[] pageCounts = fx.getMemory().getMemoryPageUsage();
			for (int i=0;i<8;i++) {
				for (int j=0;j<32;j++) {
					fill(0,0,pageCounts[i*32+j]);
					//if (pageCounts[i*16+j]>0) System.out.println("Page count:"+pageCounts[i*16+j]+" for page:"+(i*16+j));
					rect(xOffset+j*8,yOffset+i*8,8,8);
				}
			}
			
			yOffset+=8*8+10;
			//show memory blocks
			line(xOffset, yOffset, xOffset+256, yOffset);
			noStroke();
			colorMode(HSB,360,100,100);
			ArrayList<MemoryBlock> blocks = fx.getMemory().getBlocks();
			if (blocks!=null && blocks.size()>0) {
				yOffset+=5;
				int length;
				for (MemoryBlock block:blocks) {
					//if (block.blockType==MemoryBlock.FIXED) { stroke(360,100,100);} else { noStroke();}
					fill(block.blockHue,80,80);
					length = (block.blockEnd-block.blockStart)/256;
					if (length==0) length=1;
					rect(xOffset+block.blockStart/256, yOffset, length,3);
					yOffset+=5;
				}
				yOffset+=10;
				//now print memory blocks labels
				textSize(10);
				for (MemoryBlock block:blocks) {
					//if (block.blockType==MemoryBlock.FIXED) { stroke(360,100,100);} else { noStroke();}
					fill(block.blockHue,80,80);
					rect(xOffset, yOffset, 10,10);
					yOffset+=12;
					text(block.blockName+"("+String.format("%04X", block.blockStart)+"-"+String.format("%04X", block.blockEnd)+")",xOffset+12, yOffset-3);
				}
			}
			else {
				colorMode(RGB,255,255,255);
				fill(255);
				yOffset+=10;
				textSize(12);
				text("No memory segments defined.",xOffset, yOffset);
			}

			colorMode(RGB,255,255,255);
			fx.getSemaphore().release();
		}
	}

	@Override
	public PSurface getHelperSurface() {		
		return this.getSurface();
	}
	
	public void attachFx(FxSim fx) {
		this.fx = fx;
	}
		

}
