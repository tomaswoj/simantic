package com.tomaswoj.simantic.ui;

import com.tomaswoj.simantic.fx.FxSim;

import processing.core.PSurface;

public interface HelperRenderer {
	
	public String getHelperRendererName();
	public int getHelperWidth();
	public int getHelperHeight();
	public PSurface getHelperSurface();
	public void attachFx(FxSim fx);
}
