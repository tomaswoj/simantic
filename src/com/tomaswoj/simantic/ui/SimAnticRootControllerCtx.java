package com.tomaswoj.simantic.ui;

import javafx.scene.Parent;
import javafx.stage.Stage;

public class SimAnticRootControllerCtx {
	
	private Parent root;
	private Object controller;
	private Stage stage;
	
	
	public Parent getRoot() {
		return root;
	}
	public void setRoot(Parent root) {
		this.root = root;
	}
	public Object getController() {
		return controller;
	}
	public void setController(Object controller) {
		this.controller = controller;
	}
	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	
}
