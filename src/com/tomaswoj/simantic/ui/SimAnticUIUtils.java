package com.tomaswoj.simantic.ui;

import java.io.IOException;
import java.net.URL;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import processing.core.PSurface;

public class SimAnticUIUtils {
	
	public static final void removeExitEvent(final PSurface surf) {
		
		  com.jogamp.newt.opengl.GLWindow newtCanvas = (com.jogamp.newt.opengl.GLWindow) surf.getNative();
		    // Remove listeners added by Processing
		    for (com.jogamp.newt.event.WindowListener l : newtCanvas.getWindowListeners())
		      if (l.getClass().getName().startsWith("processing"))
		        newtCanvas.removeWindowListener(l);
		
	}
	
	public static void openUIWindow(Parent root, String caption) {
		  Scene scene = new Scene(root);
		  Stage addStage = new Stage();
		  addStage.setScene(scene);
		  addStage.setTitle(caption);
		  addStage.show();
	}

	public static void openUIWindow(SimAnticRootControllerCtx ctx, String caption) {
		  Scene scene = new Scene(ctx.getRoot());
		  Stage addStage = new Stage();
		  ctx.setStage(addStage);
		  addStage.setScene(scene);
		  addStage.setTitle(caption);
		  addStage.show();

	}


	public static SimAnticRootControllerCtx ctxLoader(URL location) {
		SimAnticRootControllerCtx ctx = new SimAnticRootControllerCtx();
		 Parent root=null;
		 FXMLLoader loader = new FXMLLoader();
		 try {
			  loader.setLocation(location);
			  root = loader.load();
		 } catch (IOException e) { e.printStackTrace();}
		  
		 ctx.setRoot(root); 
		 ctx.setController(loader.getController());
		 return ctx;
	}
}
