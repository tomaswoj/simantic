package com.tomaswoj.simantic.hw;

import com.tomaswoj.simantic.asm.MMap;

import processing.core.PApplet;
import processing.core.PShape;

public class Antic {
	public Memory memory;
	public ColorRGB[] coltable;	
	
	public int osMode;
	private int pixMul=1; // make it 4x4 by default
	private PShape[] fonts;
	int oldChBase=-1;
	int[] oldColors= new int[5];
	int customScreenAddress=-1;
	
	public Antic() {
		osMode = OsMode.TEXT4; // make TEXT4 a default OS mode
		for (int i=0;i<5;i++) {
			oldColors[i]=-1;
		}
	}
	
	public class OsMode {
		//https://www.atariarchives.org/agagd/chapter1.php
		public static final int TEXT = 0; // 40x24, default text
		public static final int HIRES = 8; // 320x192, monochrome
		public static final int GTIA9 = 9; // 80x192, 16 shades of background, same hue
		public static final int GTIA10 = 10; // 80x192, 9 colors
		public static final int GTIA11 = 11; // 80x192, 15 hues + background, same luminance
		public static final int TEXT4 = 12; // 40x24, 4 colors per char
		
	}
	
	public void setMemory(Memory memory) {
		this.memory=memory;
		initDefaultColors(); // for now on memory attachement...
		initDefaultVectors(); // eg fonts, future: pmbase
		forceFontReload();
	}
	
	public void initDefaultColors() {
		if (memory!=null) {
			//Register      Color  =  Hue  Luminance
			//	     708  (CO.0)     40       2       8
			//	     709  (CO.1)    202      12      10
			//	     710  (CO.2)    148       9       4
			//	     711  (CO.3)     70       4       6
			//	     712  (CO.4)      0       0       0
			memory.bytes[MMap.COLOR0]=40;
			memory.bytes[MMap.COLOR1]=202;
			memory.bytes[MMap.COLOR2]=148;
			memory.bytes[MMap.COLOR3]=70;
			memory.bytes[MMap.COLOR4]=0; // COLBACK
		}
	}
	
	public void initDefaultVectors() {
		if (memory!=null) {
			memory.bytes[MMap.CHBAS]=0xe0;
		}
	}
		
	public void draw(PApplet p) {
		switch (osMode) {
			case OsMode.TEXT:
				drawText(p);
				break;
			case OsMode.HIRES:
				drawHires(p);
			break;
			case OsMode.TEXT4:
				drawTextColor(p);
				break;
			case OsMode.GTIA9:
				drawGTIA(p,9);				
				break;
			case OsMode.GTIA10:
				drawGTIA(p,10);				
				break;
			case OsMode.GTIA11:
				drawGTIA(p,11);				
				break;				
			default:
				drawBlank(p);
		}
	}
	
	public void updateSavmsc() {
		//sets SAVMSC to the proper value
		if (this.memory!=null) {
			memory.bytes[MMap.SAVMSC_L] = getScreenPointer()%256;
			memory.bytes[MMap.SAVMSC_H] = getScreenPointer()/256;
		}
	}
	
	public void updateRTCLOCK() {
		if (this.memory!=null) {
			memory.bytes[MMap.RTCLOCK_3]++;
			if (memory.bytes[MMap.RTCLOCK_3]>=256) {
				memory.bytes[MMap.RTCLOCK_3]=0;
				memory.bytes[MMap.RTCLOCK_2]++;
				if (memory.bytes[MMap.RTCLOCK_2]>=256) {
					memory.bytes[MMap.RTCLOCK_2]=0;
					memory.bytes[MMap.RTCLOCK_1]++;					
				}
			}
		}
	}
	
	
	public void drawBlank(PApplet p) {
		p.fill(255);
		p.text("Mode not recognized... :(", 20, 20);
	}


	public boolean isFontReload() {
		boolean result = false;
		int newChBase = memory.bytes[MMap.CHBAS];
		if (oldChBase!=newChBase)
		{
			oldChBase = newChBase;
			result = true;
		}
		//now check colors
		if (oldColors[0]!=memory.bytes[MMap.COLOR0]) {
			oldColors[0]=memory.bytes[MMap.COLOR0];
			result=true;
		}
		if (oldColors[1]!=memory.bytes[MMap.COLOR1]) {
			oldColors[1]=memory.bytes[MMap.COLOR1];
			result=true;
		}
		if (oldColors[2]!=memory.bytes[MMap.COLOR2]) {
			oldColors[2]=memory.bytes[MMap.COLOR2];
			result=true;
		}
		if (oldColors[3]!=memory.bytes[MMap.COLOR3]) {
			oldColors[3]=memory.bytes[MMap.COLOR3];
			result=true;
		}
		if (oldColors[4]!=memory.bytes[MMap.COLOR4]) {
			oldColors[4]=memory.bytes[MMap.COLOR4];
			result=true;
		}
		return result;
	}
	
	//e.g. data changed (e.g. color, etc.), but CHBASE stay
	public void forceFontReload() {
		oldChBase=-1;
	}
	
	public void prepareFonts(PApplet p) {
		System.out.println("Preparing fonts for mode:"+osMode);
		switch(osMode) {
			case OsMode.TEXT:
				prepareFontsMono(p);
				break;
			case OsMode.TEXT4:
				prepareFontsColor(p);
				break;
			default:
				prepareFontsMono(p);
		}
	}
	
	public void prepareFontsMono(PApplet p) {		
		ColorRGB colback = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]&0b11110000); //COL2 sets the background color
		int colbackInt = memory.bytes[MMap.COLOR2]&0b11110000;
		ColorRGB colfor = GtiaRGB.gtia2RGB(((memory.bytes[MMap.COLOR2]&0b11110000)>>>4)*16+(memory.bytes[MMap.COLOR1]&0b11110000>>>4)); //COL1 sets the luminance for foreground color
		System.out.println("colfor:"+colfor.red+","+colfor.green+","+colfor.blue);
		int colforInt = ((memory.bytes[MMap.COLOR2]&0b11110000)>>>4)*16+(memory.bytes[MMap.COLOR1]&0b11110000>>>4);
		System.out.println("Text mode fonts colors [2],[1]:"+colbackInt+","+colforInt);
		//int fontOffset = 0xe000; //default offset
		int fontOffset = memory.bytes[MMap.CHBAS]*256; //use current offset
		//initialize fonts
		//System.out.println("Adding fonts...");
		PShape texelShape;
		fonts = new PShape[128];
		for (int i=0;i<128;i++) {
			fonts[i]= p.createShape(p.GROUP);				
			fonts[i].beginShape();
			//fonts[i].fill(colfor.red, colfor.green, colfor.blue);
			for (int line=0;line<8;line++) {
				int pixval = memory.bytes[fontOffset];
				if ((pixval&128)>0) {					
					texelShape = p.createShape(PApplet.RECT, 0,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&64)>0) {
					texelShape = p.createShape(PApplet.RECT, 1,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&32)>0) {
					texelShape = p.createShape(PApplet.RECT, 2,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&16)>0) {
					texelShape = p.createShape(PApplet.RECT, 3,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&8)>0) {
					texelShape = p.createShape(PApplet.RECT, 4,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&4)>0) {
					texelShape = p.createShape(PApplet.RECT, 5,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&2)>0) {
					texelShape = p.createShape(PApplet.RECT, 6,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				if ((pixval&1)>0) {
					texelShape = p.createShape(PApplet.RECT, 7,line,1,1);
					texelShape.setFill(p.color(colfor.red, colfor.green,colfor.blue));
					fonts[i].addChild(texelShape);					
				}
				fontOffset++;
			}
			//System.out.print("F");
			//fonts[i].endShape();
		}
		//System.out.println("Fonts added!");
		
	}
	
	public void prepareFontsColor(PApplet p) {
		ColorRGB[] colTab = new ColorRGB[4];
		colTab[0] = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLBACK]); //COLBACK sets the background color
		colTab[1] = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR0]); //COL0 sets the 01 color
		colTab[2] = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR1]); //COL1 sets the 10 color
		colTab[3] = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]); //COL2 sets the 11 color
		ColorRGB colfor = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]*16+memory.bytes[MMap.COLOR1]); //COL1 sets the luminance for foreground color

		//int fontOffset = 0xe000; //default offset
		int fontOffset = memory.bytes[MMap.CHBAS]*256; //use current offset
		//initialize fonts
		System.out.println("Adding color fonts...");
		fonts = new PShape[128];
		PShape texelShape;
		for (int i=0;i<128;i++) {
			fonts[i]= p.createShape(p.GROUP);				
			fonts[i].beginShape();
			for (int line=0;line<8;line++) {
				int pixval = memory.bytes[fontOffset];
				int colval=0;
				//take 'last pixel'
				colval = pixval & 3; //0b00000011
				texelShape = p.createShape(PApplet.RECT, 6,line,2,1);
				texelShape.setFill(p.color(colTab[colval].red, colTab[colval].green, colTab[colval].blue));
				fonts[i].addChild(texelShape);
				colval = pixval & 12; //0b00001100
				colval = colval >>> 2;
				texelShape = p.createShape(PApplet.RECT, 4,line,2,1);
				texelShape.setFill(p.color(colTab[colval].red, colTab[colval].green, colTab[colval].blue));
				fonts[i].addChild(texelShape);
				colval = pixval & 48; //0b00110000
				colval = colval >>> 4;
				texelShape = p.createShape(PApplet.RECT, 2,line,2,1);
				texelShape.setFill(p.color(colTab[colval].red, colTab[colval].green, colTab[colval].blue));
				fonts[i].addChild(texelShape);
				colval = pixval & 192; //0b00110000
				colval = colval >>> 6;
				texelShape = p.createShape(PApplet.RECT, 0,line,2,1);
				texelShape.setFill(p.color(colTab[colval].red, colTab[colval].green, colTab[colval].blue));
				fonts[i].addChild(texelShape);
				fontOffset++;
			}
			System.out.print("F");
			//fonts[i].endShape();
		}
		System.out.println("Fonts added!");
		
	}

	public void drawText(PApplet p) {		
		//if (fonts==null ) {
		if (this.isFontReload()) {
			System.out.println("Font reload");
			prepareFonts(p);
		}
		int rasters = 192;
		p.noStroke();
		ColorRGB colback = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]); //COL2 sets the background color
		p.fill(colback.red, colback.green, colback.blue);
		p.rect(0, 0, 320*getPixMul(), 192*getPixMul());
		int textVal;
		//now iterate through the screen memory
		int memoryOffset = getScreenPointer();
		for (int y=0;y<24;y++) {
			for (int x=0;x<40;x++ ) {
				textVal = memory.bytes[memoryOffset];
				//if (textVal==0x45) System.out.println("Drawing char...");				
				p.shape(fonts[textVal], 8*x*pixMul, 8*y*pixMul);				
				memoryOffset++;
			}
		}			
	}

	public void drawTextColor(PApplet p) {		
		//if (fonts==null ) {
		if (this.isFontReload()) {
			prepareFonts(p);
		}
		int rasters = 192;
		p.noStroke();
		ColorRGB colback = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLBACK]); //COL2 sets the background color
		p.fill(colback.red, colback.green, colback.blue);
		p.rect(0, 0, 320*getPixMul(), 192*getPixMul());
		int textVal;
		//now iterate through the screen memory
		int memoryOffset = getScreenPointer();
		for (int y=0;y<24;y++) {
			for (int x=0;x<40;x++ ) {
				textVal = memory.bytes[memoryOffset];
				p.shape(fonts[textVal], 8*x*pixMul, 8*y*pixMul);
				memoryOffset++;
			}
		}			
	}

	public void drawHires(PApplet p) {		
		ColorRGB colback = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]); //COL2 sets the background color
		ColorRGB colfor = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]*16+memory.bytes[MMap.COLOR1]); //COL1 sets the luminance for foreground color
		int rasters = 192;
		p.noStroke();
		p.fill(colback.red, colback.green, colback.blue);
		p.rect(0, 0, 320*getPixMul(), 192*getPixMul());
		p.fill(colfor.red, colfor.green, colfor.blue);
		int pixval;
		//now iterate through the screen memory
		int memoryOffset = getScreenPointer();
		for (int y=0;y<rasters;y++) {
			for (int x=0;x<40;x++ ) {
				pixval = memory.bytes[memoryOffset];
				if ((pixval & 128)>0) p.rect(x*8*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul); 
				if ((pixval & 64)>0) p.rect(x*8*pixMul+1*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 32)>0) p.rect(x*8*pixMul+2*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 16)>0) p.rect(x*8*pixMul+3*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 8)>0) p.rect(x*8*pixMul+4*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 4)>0) p.rect(x*8*pixMul+5*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 2)>0) p.rect(x*8*pixMul+6*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				if ((pixval & 1)>0) p.rect(x*8*pixMul+7*pixMul,y*1*pixMul, 1*pixMul, 1*pixMul);
				memoryOffset++;
			}
		}	
		
	}
	
	public void drawGTIA(PApplet p, int gtiaMode) {
		//System.out.println("Drawing GTIA9:"+memory.bytes[MMap.RTCLOCK_3]);
		//setup colors (GTIA9 is 16 shades of background color)		
		int colback = memory.bytes[MMap.COLBACK];
		int rasters = 192;
		int pixval;
		coltable = new ColorRGB[16];
		if (gtiaMode==9) {
			// 16 shades of background color
			colback*=16;
			for (int i=0;i<16;i++) {
				coltable[i]=GtiaRGB.gtia2RGB(colback+i);
			}
		}
		if (gtiaMode==10) {
			// 9 colors (704 - for back, rest of registers for all the other colors) 
			coltable[0]=GtiaRGB.gtia2RGB(memory.bytes[MMap.PCOLR0]);
			coltable[1]=GtiaRGB.gtia2RGB(memory.bytes[MMap.PCOLR1]);
			coltable[2]=GtiaRGB.gtia2RGB(memory.bytes[MMap.PCOLR2]);
			coltable[3]=GtiaRGB.gtia2RGB(memory.bytes[MMap.PCOLR3]);
			coltable[4]=GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR0]);
			coltable[5]=GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR1]);
			coltable[6]=GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR2]);
			coltable[7]=GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR3]);
			coltable[8]=GtiaRGB.gtia2RGB(memory.bytes[MMap.COLOR4]);
			for (int i=9;i<16;i++) {
				coltable[i]=coltable[0];
			}
		}
		if (gtiaMode==11) {
			// 16 hues of the same luminance (of background color, COLBACK712) 
			for (int i=0;i<16;i++) {				
				coltable[i]=GtiaRGB.gtia2RGB(i*16+colback);
			}
		}

		//draw background
		p.noStroke();
		p.fill(coltable[0].red, coltable[0].green, coltable[0].blue);
		p.rect(0, 0, 40*8*getPixMul(), 24*8*getPixMul());
		//now iterate through the screen memory
		int memoryOffset = getScreenPointer();
		for (int y=0;y<rasters;y++) {
			for (int x=0;x<40;x++ ) {
				// each byte are 2 pixels (4x1)
				// fetch 'left' pixel
				pixval = memory.bytes[memoryOffset];
				pixval = 0xF0 & pixval;
				pixval = pixval>>>4;
				p.fill(coltable[pixval].red,coltable[pixval].green,coltable[pixval].blue);
				p.rect(x*8*pixMul,y*1*pixMul, 4*pixMul, 1*pixMul);
				// fetch 'right' pixel
				pixval = memory.bytes[memoryOffset];
				pixval = 0x0F & pixval;
				p.fill(coltable[pixval].red,coltable[pixval].green,coltable[pixval].blue);
				p.rect(x*8*pixMul+4*pixMul,y*1*pixMul, 4*pixMul, 1*pixMul);
				memoryOffset++;
			}
		}	
	}
	
	public void drawTEXT4(PApplet p) {
		//System.out.println("Drawing TEXT4:"+memory.bytes[MMap.RTCLOCK_3]);
		//draw background
		ColorRGB colback = new ColorRGB();
		ColorRGB pixel = new ColorRGB();
		colback = GtiaRGB.gtia2RGB(memory.bytes[MMap.COLBACK]);
		p.fill(colback.red, colback.green, colback.blue);
		p.rect(0, 0, 40*8*getPixMul(), 24*8*getPixMul());
		p.noStroke();
		//draw memory
		//assume 40x24 for now
		int sOffset = getScreenPointer();
		for (int y=0;y<24;y++) {
			for (int x=0;x<40;x++) {
				if (memory.bytes[sOffset]>0) {
					pixel = GtiaRGB.gtia2RGB(memory.bytes[sOffset]);
					p.fill(pixel.red, pixel.green, pixel.blue);
					p.rect(8*x*getPixMul(), 8*y*getPixMul(), 8*getPixMul(), 8*getPixMul());
				}
				sOffset++;
			}
		}
	}
	
	public void putpixel(int x, int y, int col) {
		switch(osMode) {
			case OsMode.HIRES:
				putpixelHires(x,y); // ignore the color, just set the right bit
				break;
			case OsMode.GTIA9:
				putpixelGTIA(x,y,col);
				break;
			case OsMode.GTIA10:
				putpixelGTIA(x,y,col);
				break;
			case OsMode.GTIA11:
				putpixelGTIA(x,y,col);
				break;
			default:
				//do nothing
		}
	}
	
	public void putpixelGTIA(int x, int y, int col) {
		//System.out.println("Putpix");
		int screenOffset = this.getScreenPointer();
		int memOffset = y*40+x/2;
		//trim color to 4 bits only
		int colVal = col & 0b00001111;
		//memory.bytes[screenOffset+memOffset]=colVal;
		//now find the right pixel (left or right)		
		if (x%2==0) {
			//left
			colVal = colVal << 4;			
			memory.bytes[screenOffset+memOffset] |=colVal;
		}
		
		if (x%2==1) {					
			memory.bytes[screenOffset+memOffset] |=colVal;
		}
	}
	
	public void putpixelHires(int x, int y) {
		//find the right byte
		int screenOffset = this.getScreenPointer();
		int memOffset = y*40+x/8;
		//now find the right pixel
		int pixelOffset = x%8;
		if (pixelOffset==0) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b10000000; 
		if (pixelOffset==1) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b01000000;
		if (pixelOffset==2) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00100000;
		if (pixelOffset==3) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00010000;
		if (pixelOffset==4) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00001000;
		if (pixelOffset==5) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00000100;
		if (pixelOffset==6) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00000010;
		if (pixelOffset==7) memory.bytes[screenOffset+memOffset] = memory.bytes[screenOffset+memOffset] | 0b00000001;
	}
	
	
	public int getAnticWidth() {
		int result =0;
		result = 320 * pixMul;
		return result;
	}
	
	public int getAnticHeight() {
		int result = 0;
		result = 192 *pixMul;
		return result;
	}
	
	public void setMode(int osMode) {
		this.osMode = osMode;
		System.out.println("Set fx mode to:"+osMode);
	}
	
	public void setCustomScreenAddress(int naddress) {
		customScreenAddress = naddress;
	}
	
	public int getScreenPointer() {		
		int screenPointer = 0;
		if (customScreenAddress>0) {
			screenPointer = customScreenAddress;
		}
		else 
		{
			switch(osMode) {
				case OsMode.TEXT:
					screenPointer = 0x9c40;
					break;
				case OsMode.HIRES:
					screenPointer = 0xa150;
					break;
				case OsMode.GTIA9:
					screenPointer = 0xa150;					
					break;
				case OsMode.GTIA10:
					screenPointer = 0xa150;
					break;
				case OsMode.GTIA11:
					screenPointer = 0xa150;
					break;			
				case OsMode.TEXT4:
					screenPointer = 0xbba0;
					break;
				default:
					screenPointer = 0xbba0;
			}
		}
		return screenPointer;
	}

	public int getPixMul() {
		return pixMul;
	}

	public void setPixMul(int pixMul) {
		this.pixMul = pixMul;
	}
	
}
