package com.tomaswoj.simantic.hw;

public class GtiaRGB {
	
	public static ColorRGB gtia2RGB(int val) {
		ColorRGB rgb = new ColorRGB();		
		//int cr = (val >> 4) & 15;
		int cr = (val / 16) & 15;
		int lm = val & 15;
		int crlv;
		if (cr==0) crlv =0;
		else crlv=50;
		//int crlv = cr ? 50 : 0;
	
		double phase = ((cr-1)*25.7 - 15) * (2 * 3.1416 / 360);
		//System.out.println("Phase:"+phase);

		double y = 255*(lm+1)/16;
		double i = crlv*Math.cos(phase);
		double q = crlv*Math.sin(phase);

		double r = y + 0.956*i + 0.621*q;
		double g = y - 0.272*i - 0.647*q;
		double b = y - 1.107*i + 1.704*q;

		if (r<0) r=0;
		if (r>255) r=255;
		if (g<0) g=0;
		if (g>255) g=255;
		if (b<0) b=0;
		if (b>255) b=255;

		rgb.red= (int) r;		
		rgb.green = (int) g;
		rgb.blue = (int) b;
		return rgb;
		}
}
