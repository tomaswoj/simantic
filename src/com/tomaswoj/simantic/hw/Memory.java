package com.tomaswoj.simantic.hw;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.tomaswoj.simantic.asm.Asm;

import processing.core.PApplet;

public class Memory {
	
	public static final int MEMBANK_START=0x4000;
	public static final int MEMBANK_END=0x7FFF;
	
	public int memSize;
	public int[] bytes;
	public int[] bytesA0; // base RAM memory, backup buffer 
	public int[] bytesA1; // additional 4x16K memory banks (like in base Atari 130XE)
	public int[] bytesA2;
	public int[] bytesA3;
	public int[] bytesA4;
	private boolean romPresent=false;
	
	private ArrayList<MemoryBlock> memoryBlocks;
	private int[] hueList = new int[] {180, 1, 30, 60, 120, 180, 200, 240, 280, 300};
	private int hueIndex=0;
	private int hueOffset=0;
	private int currentBank=0; //start with current bank in RAM
	
	public class MemoryBlock {
		public static final int FIXED=0;
		public static final int GENERATED=1;		
		public int blockStart;
		public int blockEnd;
		public int blockType;
		public String blockName;
		public int blockHue;
	}
		
		
	public Memory() {
		memSize=65536;
		bytes = new int[memSize];
		int bytesBank = memSize/4;
		bytesA0 = new int[bytesBank];
		bytesA1 = new int[bytesBank];
		bytesA2 = new int[bytesBank];
		bytesA3 = new int[bytesBank];
		bytesA4 = new int[bytesBank];
		memoryBlocks = new ArrayList<>();
	}
	
	public void addMemoryBlock(String name, int start, int end, int type) {
		MemoryBlock newBlock = new MemoryBlock();
		newBlock.blockStart=start;
		newBlock.blockEnd=end;
		newBlock.blockType=type;
		newBlock.blockName=name;
		//newBlock.blockHue = (int) (Math.random()*360);
		newBlock.blockHue=hueList[hueIndex]+hueOffset;
		memoryBlocks.add(newBlock);
		hueIndex++;
		if (hueIndex==hueList.length) {
			hueIndex=0;
			hueOffset+=10;
		}

	}
	
	public ArrayList<MemoryBlock> getBlocks() {
		return memoryBlocks;
	}
	
	public int peek(int address) {
		if (address<memSize && address>=0)
		return bytes[address];
		else return -1;
	}
	
	public int peek_word(int address) {
		if (address<memSize && address>=0) {
			//System.out.println("addr:"+bytes[address]+" addr+1:"+bytes[address+1]);
			return bytes[address+1]*256+bytes[address];
		}
		else return -1;
	}
	
	public void poke(int address, int value) {
		if (address<memSize) bytes[address]=value;
	}
	
	public static int toUnsignedInt(byte x) { return ((int) x) & 0xff; }
	
	public void loadRom() {
		//loads standard AtariXL rom into the memory - $C000 onwards
		byte[] rom;
		try {
		    Path path = Paths.get("ATARIXL.ROM");
		    rom = Files.readAllBytes(path);
		    for (int i=0;i<rom.length;i++) {
		    	bytes[0xC000+i] = toUnsignedInt(rom[i]);
		    }
		} catch (IOException e) {
			System.out.println("Could not load ROM:"+e.toString());
		}
		
		romPresent=true;		
	}
	
	public void loadDatToMemory(String name, int start, int size) {
		byte[] dat;
		int loadSize=0;
		try {
		    Path path = Paths.get(name);
		    dat = Files.readAllBytes(path);
		    if (size>=dat.length) loadSize=dat.length;
		    if (dat.length>size) loadSize=size;
		    for (int i=0;i<loadSize;i++) {
		    	bytes[start+i] = toUnsignedInt(dat[i]);
		    }
		} catch (IOException e) {
			System.out.println("Could not load data file:"+e.toString());
		}		
	}
	

	public void switchBank(int newBank) {
		//internal method, assume 0 is a base RAM, 1... go for 0-3 additional banks (as in 130XE)
		if (currentBank!=newBank) {
			//copy the data from the main RAM memory area to the right bank (or backup bank in case or base RAM)
			switch(currentBank) {
				case 0:
					//main memory
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytesA0[i-MEMBANK_START]=bytes[i];
					}
					break;
				case 1:
					//additional bank 0
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytesA1[i-MEMBANK_START]=bytes[i];
					}
					break;
				case 2:
					//additional bank 1
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytesA2[i-MEMBANK_START]=bytes[i];
					}
					break;
				case 3:
					//additional bank 2
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytesA3[i-MEMBANK_START]=bytes[i];
					}					
					break;
				case 4:
					//additional bank 3
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytesA4[i-MEMBANK_START]=bytes[i];
					}
					break;
				default:
					//main memory
			}
			//copy the data from the new bank into the main memory area
			switch(newBank) {
				case 0:
					//main memory
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytes[i]=bytesA0[i-MEMBANK_START];
					}
					break;
				case 1:
					//additional bank 0
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytes[i]=bytesA1[i-MEMBANK_START];
					}
					break;
				case 2:
					//additional bank 1
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytes[i]=bytesA2[i-MEMBANK_START];
					}
					break;
				case 3:
					//additional bank 2
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytes[i]=bytesA3[i-MEMBANK_START];
					}					
					break;
				case 4:
					//additional bank 3
					for (int i=MEMBANK_START; i<=MEMBANK_END;i++)
					{
						bytes[i]=bytesA4[i-MEMBANK_START];
					}
					break;
				default:
					//main memory
			}
			
			currentBank=newBank;
		}
	}
	
	public void dumpMemoryToHexFile(PApplet p, String filename, int start, int size, int linesize) {
		  ArrayList<String> lines = new ArrayList<String>();
		  String line="";
		  for (int i=start;i<start+size;i++) {
		    if ((i-start)%linesize==0 || i==start+size-1) {
		    	if (line.length()>0 && i==start+size-1) line+=" "+String.format("%02X", bytes[i]);
		    	if (line.length()>0) lines.add(line);
		    	line=new String();		    	
		    }
		    line+=" "+String.format("%02X", bytes[i]);
		  }
		  String[] stringLines = new String[lines.size()];
		  lines.toArray(stringLines);
		  p.saveStrings(filename,stringLines);
	}
	
	public void dumpMemoryToFile(String filename, int start, int size) {		 
		DataOutputStream os;
		try {
			os = new DataOutputStream(new FileOutputStream(filename));
			 for (int i=start;i<start+size;i++) 
			 {
				 try {
					//os.writeInt(bytes[i]);
					os.writeByte(bytes[i]);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }
			 try {
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
	
			e.printStackTrace();
		}
	}
	
	
	
	public int percentageUsed() {
		int bytesUsed=0;
		int memTop = bytes.length;
		if (romPresent) memTop-=16384;
		for (int i=0;i<memTop;i++) {
			if (bytes[i]>0) bytesUsed++;
		}
		return (int)(bytesUsed*100/memTop);
	}
	
	
	public int bytesUsed() {
		int bytesUsed=0;
		int memTop = bytes.length;
		if (romPresent) memTop-=16384;
		for (int i=0;i<memTop;i++) {
			if (bytes[i]>0) bytesUsed++;
		}
		return bytesUsed;
	}
	
	public int[] getMemoryPageUsage() {
		int[] result = new int[256];
		for (int i=0;i<256;i++) {
			int pageCount=0;
			for (int j=0;j<255;j++) {
				if (bytes[i*256+j]>0) pageCount++;
			}
			result[i]=pageCount;
			//System.out.println("Page count:"+pageCount+"for page:"+Asm.printHexValByte(i));
		}
		return result;
	}
}
