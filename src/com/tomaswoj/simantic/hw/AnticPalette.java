package com.tomaswoj.simantic.hw;

public class AnticPalette {
	private static ColorRGB[] palette;
	
	public static ColorRGB[] getPalette() {
		if (palette==null) {
			palette = initializeDefaultPalette();
		}
		return palette;
	}
	
	private static ColorRGB[] initializeDefaultPalette() {
		ColorRGB[] newpal = new ColorRGB[256];
		for (int i=0;i<256;i++) {
			newpal[i] = GtiaRGB.gtia2RGB(i);			
		}
		return newpal;
	}
	
}
