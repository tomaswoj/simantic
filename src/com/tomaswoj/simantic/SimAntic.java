package com.tomaswoj.simantic;


import java.util.ArrayList;
import java.util.HashMap;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.fx.FxSim;
import com.tomaswoj.simantic.hw.AnticPalette;
import com.tomaswoj.simantic.hw.ColorRGB;
import com.tomaswoj.simantic.hw.GtiaRGB;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;

import processing.core.PApplet;


public class SimAntic extends PApplet {
	
	private static PApplet pApplet;
	private static FxSim fxSim;
	private static boolean drawPalette=false;
	private static boolean drawColRegs=false;

	
	public SimAntic() {
		super();
		SimAntic.setpApplet(this);
	}
	
	

public void settings() {
	
  SimAntic.setpApplet(this);

  size(320+20,200+20);
  
}

public void setup() {
	frameRate(60); // we don't need more than that, otherwise concurrency issues appear
	frame.setResizable(true);
}

public void resizeView(int nwidth, int nheight) {
	frame.setSize(nwidth, nheight);
}

public void draw() {		
	
	if (fxSim!=null && pApplet!=null) {
		background(0);

		pApplet.translate(10, 10);
		fxSim.drawFx(pApplet);
		pApplet.translate(-10, -10);
		
		//check if status to be shown
		if (fxSim.isShowStatus()) {
			pApplet.textSize(10);
			pApplet.fill(255);
			int fps = (int) pApplet.frameRate;
			pApplet.text("Processing FPS:"+fps, 10, 10);
			pApplet.text("SimAntic Mem:"+fxSim.getMemory().bytesUsed()/1024+"kB Cycles:"+fxSim.getAsm().getCount()/1000+"k", 10, 25);
			
			/*
			//show section counts
			int yOffset=20;
			int xOffset=20+320+20;
			pApplet.text("Cycles counts:", xOffset, 12);
			HashMap<String,Integer> counts = fxSim.getAsm().getSectionCounts();			
			for (String section:counts.keySet()) {
				pApplet.text(section, xOffset, 10+yOffset);
				//draw a bar
				pApplet.rect(xOffset+60, 10+yOffset-10, (counts.get(section)/1000), 10);
				pApplet.text((counts.get(section)/1000)+"K", xOffset+60+5+(counts.get(section)/1000), 10+yOffset);
				yOffset+=12;
			}
			
			//show memory map (usage)
			stroke(255);
			yOffset=140;			
			int[] pageCounts = fxSim.getMemory().getMemoryPageUsage();
			for (int i=0;i<8;i++) {
				for (int j=0;j<32;j++) {
					fill(0,0,pageCounts[i*16+j]);
					rect(xOffset+j*8,yOffset+i*8,8,8);
				}
			}
			
			yOffset+=8*8+10;
			//show memory blocks
			line(xOffset, yOffset, xOffset+256, yOffset);
			noStroke();
			colorMode(HSB,360,100,100);
			ArrayList<MemoryBlock> blocks = fxSim.getMemory().getBlocks();
			yOffset+=5;
			int length;
			for (MemoryBlock block:blocks) {
				//if (block.blockType==MemoryBlock.FIXED) { stroke(360,100,100);} else { noStroke();}
				fill(block.blockHue,80,80);
				length = (block.blockEnd-block.blockStart)/256;
				if (length==0) length=1;
				rect(xOffset+block.blockStart/256, yOffset, length,3);
				yOffset+=5;
			}
			//now print memory blocks
			textSize(8);
			for (MemoryBlock block:blocks) {
				//if (block.blockType==MemoryBlock.FIXED) { stroke(360,100,100);} else { noStroke();}
				fill(block.blockHue,80,80);
				rect(xOffset, yOffset, 8,8);
				yOffset+=10;
				text(block.blockName+"("+String.format("%04X", block.blockStart)+"-"+String.format("%04X", block.blockEnd)+")",xOffset+10, yOffset-2);
			}

			colorMode(RGB,255,255,255);
			*/
 
		}		
		/*
		if (drawPalette) {
			if (fxSim.getAntic()!=null)
			drawPalette(0,30+30+fxSim.getAntic().getAnticHeight());			
			else {
				System.out.println("antic null!");
			}
		}
		if (isDrawColRegs()) {
			if (drawPalette) {
				drawColRegs(240,30+30+fxSim.getAntic().getAnticHeight());
				drawActualPalette(240,30+30+fxSim.getAntic().getAnticHeight()+60);
			}
			else {
				drawColRegs(20,30+30+fxSim.getAntic().getAnticHeight());
				drawActualPalette(20,30+30+fxSim.getAntic().getAnticHeight()+60);
			}
		}
		*/
		
	}
}

public void drawPalette(int offsetX, int offsetY) {
	//get palette from AnticPalette (default for now)
	ColorRGB[] colors = AnticPalette.getPalette();
	int posX = offsetX+20;
	int posY = offsetY;
	int paletteOffset=0;
	noStroke();	
	// top text row
	textSize(8);
	fill(255);
	for (int i=0;i<16;i++) {
		text(String.format("%01X", i), posX+i*12+12, posY);		
	}
	for (int i=0;i<16;i++) {
		text(String.format("%01X", i), posX, posY+i*12+12);		
	}
	posY+=3;
	posX+=12;
	for (int i=0;i<16;i++) {
		for (int j=0;j<16;j++) {
			fill(colors[paletteOffset].red, colors[paletteOffset].green, colors[paletteOffset].blue);
			rect(posX+j*12, posY+i*12, 9,9);
			paletteOffset++;
		}
	}
}

public void drawColRegs(int offsetX, int offsetY) {
	textSize(8);
	noStroke();
	ColorRGB col0 = GtiaRGB.gtia2RGB(fxSim.getMemory().bytes[MMap.COLOR0]);
	ColorRGB col1 = GtiaRGB.gtia2RGB(fxSim.getMemory().bytes[MMap.COLOR1]);
	ColorRGB col2 = GtiaRGB.gtia2RGB(fxSim.getMemory().bytes[MMap.COLOR2]);
	ColorRGB col3 = GtiaRGB.gtia2RGB(fxSim.getMemory().bytes[MMap.COLOR3]);
	ColorRGB col4 = GtiaRGB.gtia2RGB(fxSim.getMemory().bytes[MMap.COLOR4]);
	fill(255);
	text("COL0: $"+String.format("%02X", fxSim.getMemory().bytes[MMap.COLOR0]), offsetX, offsetY+6);
	text("COL1: $"+String.format("%02X", fxSim.getMemory().bytes[MMap.COLOR1]), offsetX, offsetY+18);
	text("COL2: $"+String.format("%02X", fxSim.getMemory().bytes[MMap.COLOR2]), offsetX, offsetY+30);
	text("COL3: $"+String.format("%02X", fxSim.getMemory().bytes[MMap.COLOR3]), offsetX, offsetY+42);
	text("COL4: $"+String.format("%02X", fxSim.getMemory().bytes[MMap.COLOR4]), offsetX, offsetY+54);
	fill(col0.red, col0.green, col0.blue);
	rect(offsetX+50, offsetY, 9, 9);
	fill(col1.red, col1.green, col1.blue);
	rect(offsetX+50, offsetY+12, 9, 9);
	fill(col2.red, col2.green, col2.blue);
	rect(offsetX+50, offsetY+24, 9, 9);
	fill(col3.red, col3.green, col3.blue);
	rect(offsetX+50, offsetY+36, 9, 9);
	fill(col4.red, col4.green, col4.blue);
	rect(offsetX+50, offsetY+48, 9, 9);
	
}
public void drawActualPalette (int offsetX, int offsetY) {
	if (fxSim.getAntic().coltable!=null)
	for (int i=0;i<16;i++) {
		fill(fxSim.getAntic().coltable[i].red,
			 fxSim.getAntic().coltable[i].green,
			 fxSim.getAntic().coltable[i].blue);
		rect(offsetX+12*i, offsetY, 9,9);
	}
}

public void mouseClicked() {
	  int mx = mouseX;
	  int my = mouseY;
	}

	public static PApplet getpApplet() {
		return pApplet;
	}

	public static void setpApplet(PApplet pApplet) {
		SimAntic.pApplet = pApplet;
	}



	public static FxSim getFxSim() {
		return fxSim;
	}



	public static void setFxSim(FxSim fxSim) {
		SimAntic.fxSim = fxSim;
	}



	public static boolean isDrawPalette() {
		return drawPalette;
	}



	public static void setDrawPalette(boolean drawPalette) {
		SimAntic.drawPalette = drawPalette;
	}

	public static boolean isDrawColRegs() {
		return drawColRegs;
	}

	public static void setDrawColRegs(boolean drawColRegs) {
		SimAntic.drawColRegs = drawColRegs;
	}


}