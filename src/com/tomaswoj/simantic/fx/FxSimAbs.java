package com.tomaswoj.simantic.fx;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import com.tomaswoj.simantic.asm.Asm;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;

import processing.core.PApplet;

public abstract class FxSimAbs implements FxSim {
	protected Asm asm;
	private int runCredits;
	protected ArrayList<String> offsets;
	private boolean showStatus=false;
	private boolean drawHelper=false;
	
	private Semaphore semaphore;
	
	public FxSimAbs() {
		semaphore = new Semaphore(1);
		initAsm();
		initOffsets();
		initFxSim();
	}
	
	public void initAsm() {
		//override if needed
		asm = Asm.getAsm64k(); // get default asm, setup default osMode		
	}
	
	public void initOffsets() {
		offsets = new ArrayList<>();
		offsets.add("Zero Page:0x0000");
		offsets.add("Stack Page:0x0100");
		offsets.add("Color shadows:0x02c4");
	}
	
	public void playWhole() {
		runCredits = 65535;
	}
	
	public void playStep() {
		runCredits = 1;
	}
	
	public void playCredits(int credits) {
		runCredits = credits;
	}

	public void drawFx(PApplet p)
	{
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (runCredits>0) {
			asm.antic.updateSavmsc();
			stepFx();
		}
		asm.antic.draw(p); // draw anyway, even if paused
		//if (drawHelper) {
		//	drawHelperView(p);
		//}
		if (runCredits>0) {
			asm.antic.updateRTCLOCK();
			runCredits--;
		}
		semaphore.release();
	}
	
	public void drawHelperView(PApplet p) {
		//override if you want to draw some helper overlay...
	}
	
	public Cpu getCpu() {
		return asm.cpu;
	}
	
	public Antic getAntic() {
		return asm.antic;
	}
	
	public Asm getAsm() {
		return asm;
	}
	
	public ArrayList<String> getOffsets() {
		return offsets; 
	}

	public boolean isShowStatus() {
		return showStatus;
	}

	public void setShowStatus(boolean showStatus) {
		this.showStatus = showStatus;
	}
	
	public boolean isDrawHelper() {
		return drawHelper;
	}
	
	public void setDrawHelper(boolean drawHelper) {
		this.drawHelper=drawHelper;
	}
	
	public Semaphore getSemaphore() {
		return semaphore;
	}
	
	public int getHelperWidth() {
		//override if needed
		return 320;
	}
	
	public int getHelperHeight() {
		//override if needed
		return 320;
	}
	
	public void addBlock(String name, int start, int end, int type) {
		//add memory block
		asm.mem.addMemoryBlock(name, start, end, type);
		//add corresponding offset
		offsets.add(name+":"+"0x"+Asm.printHexValWord(start));		
	}
}
