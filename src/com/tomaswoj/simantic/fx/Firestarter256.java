package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;

import processing.core.PApplet;

import com.tomaswoj.simantic.hw.Antic.OsMode;

public class Firestarter256 extends FxSimAbs {


	private int[] charsetData = new int[] {			
			0,0,0,0,0,0,0,0,
			68,0,17,0,68,0,17,0,
			68,17,0,68,17,0,68,17,
			68,17,85,68,17,85,68,17,
	      	17,85,68,85,17,85,68,85,
	      	85,85,85,85,85,85,85,85,
	      	153,85,102,85,153,85,102,85,
	      	153,102,85,153,102,85,153,102,
	      	153,102,170,153,102,170,153,102,
	      	102,170,153,170,102,170,153,170,
	      	170,170,170,170,170,170,170,170,
	      	238,170,187,170,238,170,187,170,
	      	238,187,170,238,187,170,238,187,
	      	238,187,255,238,187,255,238,187,
	      	238,255,187,255,238,255,187,255,
	      	255,255,255,255,255,255,255,255};

	@Override
	public String getFxName() {		
		return "Firestarter 256b";
	}

	public Firestarter256() {
		super();
		offsets.add("ScreenMem:0xbba0");
		initFx();
	}
	@Override
	public void initFxSim() {
		//nothing special here
		
	}

	@Override
	public void drawHelperView(PApplet p) {
		//just a dummy helper
		p.noFill();
		p.stroke(255);
		p.rect(10, 10, 100, 100);
		p.line(10, 10, 110, 110);
	}
	
	@Override
	public void initFx() {
		//load rom
		asm.mem.loadRom();
		// setup color
		asm.antic.setMode(OsMode.TEXT4); //16 scales of backcolor
		//asm.antic.setMode(OsMode.GTIA11); //16 hues of the backcolor luminance
		//asm.lda_imm(0x94);
		//asm.sta(MMap.COLOR2); // blue
		//asm.lda_imm(0xca);
		//asm.sta(MMap.COLOR1); // blue
		
		//copy original charset
		//for (int i=0;i<4*256;i++) {
		//	asm.lda(0xe000+i);
		//	asm.sta(0x7000+i);
		//}
		//copy dither chars
		for (int i=0;i<charsetData.length;i++) {
			asm.lda_imm(charsetData[i]);
			asm.sta(0x7030+i);
		}
		//now set chbas
		asm.lda_imm(0x70);
		asm.sta(MMap.CHBAS);

	}

	@Override
	public void stepFx() {
		// fire effect
		for (int i=asm.antic.getScreenPointer();i<asm.antic.getScreenPointer()+40*24;i++) {
		//	// formula: i=1/2 *(i+39) + 2*(i+40) + 1/2*(i+40) + 1*(i+80)  
			int val = (asm.mem.bytes[i+39] +asm.mem.bytes[i+41])/2 + 2*asm.mem.bytes[i+40]+asm.mem.bytes[i+80];			
			val/=4;
			if (val>19) val=19;
			asm.mem.bytes[i]=val;
		}
		// randomize bottom
		for (int i=asm.antic.getScreenPointer()+40*23;i<asm.antic.getScreenPointer()+40*24;i++) {
			int randVal = (int)(Math.random()*255); 
			if (randVal>=128) randVal=127;
			asm.lda_imm(randVal);
			asm.sta(i);
		}
		if (asm.mem.bytes[MMap.RTCLOCK_3]==120) {
			//randomize all the colors
			asm.mem.bytes[MMap.COLOR0]=(int)(Math.random()*255);
			asm.mem.bytes[MMap.COLOR1]=(int)(Math.random()*255);
			asm.mem.bytes[MMap.COLOR2]=(int)(Math.random()*255);
			//force font reload
			asm.antic.forceFontReload();
		}
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
