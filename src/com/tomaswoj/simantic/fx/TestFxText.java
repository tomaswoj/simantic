package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Antic.OsMode;

public class TestFxText extends FxSimAbs {

	@Override
	public String getFxName() {		
		return "TestFx Textmode (def)";
	}

	public TestFxText() {
		super();
		offsets.add("ScreenMem:0x9c40");
		initFx();
	}
	@Override
	public void initFxSim() {
		//load rom
		asm.mem.loadRom();		
	}

	@Override
	public void initFx() {
		// setup color
		asm.antic.setMode(OsMode.TEXT); // regular text mode
		asm.lda_imm(0x94);
		asm.sta(MMap.COLOR2); // blue
		asm.lda_imm(0xca);
		asm.sta(MMap.COLOR1); // blue

	}

	@Override
	public void stepFx() {
		// set some pattern on the screen...		
		asm.lda_imm(0x32); // R
		asm.sta(asm.antic.getScreenPointer()+40);
		asm.lda_imm(0x25); // E
		asm.sta(asm.antic.getScreenPointer()+41);		
		asm.lda_imm(0x21); // A
		asm.sta(asm.antic.getScreenPointer()+42);		
		asm.lda_imm(0x24); // D
		asm.sta(asm.antic.getScreenPointer()+43);		
		asm.lda_imm(0x39); // Y
		asm.sta(asm.antic.getScreenPointer()+44);				
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
