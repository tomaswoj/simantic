package com.tomaswoj.simantic.fx;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.tomaswoj.simantic.SimAntic;
import com.tomaswoj.simantic.asm.Asm;
import com.tomaswoj.simantic.asm.Loop;
import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.geometry.Point3D;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.ColorRGB;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.GtiaRGB;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Memory.MemoryBlock;
import com.tomaswoj.simantic.utils.ImageUtils;
import com.tomaswoj.simantic.hw.Antic.OsMode;

import processing.core.PApplet;
import processing.core.PImage;

public class FxVoxels extends FxSimAbs {

	
	// export in SimAntic - kamino.dat, 0x1600 - 0x2cff - 5887b dat file
	
	// export in Simantic - kamino.dat, with packed tex_addr:
	// 0x1600 - 0x29c0 - 5056b data file
	int[] mainBuff = new int[64*63];
	
	int[] skycols = new int[] {
			 0x93,0x92,0x93,0x93,0x93,0x93,0x93,0x93,0x93,0x93,
			 0x93,0x93,0x93,0x93,0x93,0x94,0x93,0x94,0x93,0x94,
			 0x93,0x94,0x94,0x94,0x93,0x94,0x94,0x94,0x94,0x94,
			 0x94,0x94,0x94,0x95,0x94,0x94,0x95,0x95,0x94,0x95,
			 0x95,0x95,0x95,0x95,0x95,0x95,0x95,0x95,0x96,0x95,
			 0x96,0x95,0x96,0x96,0x96,0x96,0x96,0x96,0x96,0x96,
			 0x96,0x97,0x96,0x97,0x96,0x97,0x97,0x97,0x97,0x97,
			 0x97,0x98,0x97,0x98,0x97,0x98,0x98,0x98,0x98,0x98,
			 0x98,0x98,0x99,0x98,0x99,0x98,0x99,0x99,0x99,0x99,
			 0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,			 			
			 0x99,0x98,0x99,0x98,0x99,0x98,0x99,0x98,0x98,0x98
			 };

	int dumpIndex=0;
	private Point3D camera;
	private Point3D[][] screenPoints;
	private int[][] screenColors;
	private PImage texture2D;
	private PImage skyback;
	private int textureSize=64; //64x64
	private int zOffset=0;
	private int xOffset=0;
	private int frameCount=0;
	private float[] hmax = new float[49]; //hmax(z->y)
	private int[] colfills = new int[24]; //count the histogram of call fills
	
	//ZEROPAGE
	private static final int PIX_VAL = 0x0080; //current pix val, on zeropage
	private static final int CMAX = 0x0081; //max common height
	private static final int TEX_L=0x0082; //texture addr pointer, low
	private static final int FILL_L=0x0082; //used in screen filler calculation 
	private static final int TEX_H=0x0083; //texture addr pointer, high
	private static final int FILL_H=0x0083; //used in screen filler calculation 
	private static final int SF_L=0x0084; //screen filler pointer, low
	private static final int SF_H=0x0085; //screen filler pointer, high
	private static final int HMAX_L=0x0086;
	private static final int SCRS_L=0x0086;
	private static final int HMAX_H=0x0087;
	private static final int SCRS_H=0x0087;
	private static final int ROW=0x0088;
	private static final int COLUMN=0x0089;
	private static final int CF_L=0x008a; //COLFIL_L 
	private static final int CF_H=0x008b; //COLFIL_H -> 0x3f
	private static final int FROM_L=0x008c; //seamove from low
	private static final int FROM_H=0x008d; //seamove from high
	private static final int TO_L=0x008e; //seamove to low
	private static final int TO_H=0x008f; //seamove to high
	private static final int SEAFR=0x0090; //seaframe
	private static final int CBANK=0x0091; //current frame bank
	private static final int CPAGE=0x0092; //current frame page 
	private static final int COLVAL=0x009f; //whole column of values (94)
	
	private static final int PREFETCH = 0x1400; //prefetch unrolled code
	private static final int TEX_DATA_PK=0x1600; //initial packed texture from 0x1600 to 0x1e00
	private static final int TEX_DATA_PKPG=0x16; //initial packed texture from 0x1600 to 0x1e00
	private static final int TEX_DATA_PKPG_END=0x1e; 
	private static final int SCR_OFFSETS = 0x3e80; //offsets and bank masks for frame data (64x1b for page + 64x1b for bank mask/identifier/portb value)
	private static final int SCR_BANKS = 0x3ec0; //offsets and bank masks for frame data (64x1b for page + 64x1b for bank mask/identifier/portb value)	
	private static final int COLFILL = 0x3e00; // column fillers (24*3bytes + JMP back) <- initial prefetch
	private static final int HMAX = 0x3d00; //we need a page of values, like: <- initial prefetch
	private static final int HMAX_PG = 0x3d; //we need a page of values, like: <- initial prefetch
	private static final int PIX_DATA = 0x5200; //pixels values copied (80x24)
	private static final int PIX_PG = 0x52; //pixels values page
	private static final int FILL_VEC = 0x4f00; // 40 columns x 2 bytes, pointing to FILLERS table
	private static final int HMAX_OFFSETS = 0x3f00; // 24 offsets <- ipref
	private static final int SKYLINE=0x3f20;
	private static final int CBASES = 0x3e65; // 24 bases <- ipref
	private static final int FILLERS = 0x5700; //fillers space 0x5000 - 0x9e20	
	private static final int FILLERS_PG = 0x57; //fillers page
	private static final int IDENTITY = 0x9f90; //16 identities
	private static final int COLFILL_VEC = 0x3e4c; // column filler offsets <- init pref
	private static final int COLFILL_VPG = 0x3e; // column filler offsets PG <- init pref
	private static final int SCR_MEM = 0xe150; //beginning of screen mem
	private static final int TEX_ADDR = 0x1e80; //in format [low/hig][low/high] -> texture pixels, new address with initial prefetcher
	private static final int TEX_ADDR_PACKED = 0xc000; 
	private static final int TEX_ADDR_UNPACKED = 0xad00;
	private static final int TEX_DATA = 0x9c00; //texture pixels, 64x64 -> texture data with initial prefetcher
	private static final int TEX_DATA_PG = 0x9c; //texture pixels, 64x64 -> with initial prefetcher
	private static final int CPY_BUFFER = 0xe000; //64 pix buffer, to copy the texture around
	
	
	@Override
	public String getFxName() {		
		return "Voxels GTIA9";
	}

	public FxVoxels() {
		super();
		initFx();
	}
	@Override
	public void initFxSim() {
		//nothing special here		
	}

	@Override
	public void initFx() {
		addBlock("Screen", 0xa150,0xbf50 , MemoryBlock.FIXED);
		addBlock("TextureAddr", 0x1e00,0x1e00+80*24*2, MemoryBlock.FIXED); //ipref
		addBlock("Texture", 0x2d00,0x2d00+4096 , MemoryBlock.FIXED); //ipref
		addBlock("Hmax Data", HMAX, HMAX+256-32, MemoryBlock.GENERATED); //ipref
		addBlock("Column Fillers", COLFILL, COLFILL+75, MemoryBlock.GENERATED); // initial pref
		addBlock("Column Fillers Vec", COLFILL_VEC, COLFILL_VEC+24, MemoryBlock.GENERATED); // initial pref
		addBlock("Frame Banks", SCR_BANKS,SCR_BANKS+64, MemoryBlock.FIXED); //ipref
		addBlock("Frame Pages", SCR_OFFSETS,SCR_OFFSETS+64, MemoryBlock.FIXED); //ipref
		addBlock("Hmax Vec", HMAX_OFFSETS, HMAX_OFFSETS+24, MemoryBlock.GENERATED);// ipref
		addBlock("CBases", CBASES, CBASES+24, MemoryBlock.GENERATED); //ipref
		addBlock("Screen Fillers Vec", 0x4f00, 0x4f50, MemoryBlock.GENERATED);
		addBlock("Screen Fillers", 0x5700, 0x5700+18448, MemoryBlock.GENERATED);
				
		int camDistance=100;
		camera = new Point3D();
		camera.x=40; camera.y=24;camera.z=0;
		screenPoints = new Point3D[48][80];
		screenColors = new int[48][80];
		for (int y=0;y<48;y++) {
			for (int x=0;x<80;x++) {
				screenPoints[y][x]=new Point3D();
				screenPoints[y][x].x=x;
				screenPoints[y][x].y=48-y;
				screenPoints[y][x].z=100;
			}
		}
		
		// setup color
		asm.antic.setMode(OsMode.GTIA9); //16 scales of backcolor
		asm.antic.setCustomScreenAddress(SCR_MEM);
		asm.lda_imm(0x09); //nice sea blue
		asm.sta(MMap.COLBACK); // sea blue scale
		//load texture image to memory
		ImageUtils.loadImageToMemPacked(SimAntic.getpApplet(), "terrain_seam.png", asm.mem, TEX_DATA_PK, 0.0625f);
		
		texture2D = SimAntic.getpApplet().loadImage("terrain.png");

		updateColorsAsmZigzag();
		packTexAddr(TEX_ADDR, TEX_ADDR_PACKED);
		copyTexAddrPacked(TEX_ADDR_PACKED, TEX_ADDR);	
		//unpackTexAddr(TEX_ADDR_PACKED, FILLERS);
		//unpackTexAddr(TEX_ADDR_PACKED, TEX_ADDR_UNPACKED);
		unpackTexAddrAsm();
		//cleanE000();
		//copyTexAddrUnpacked(TEX_ADDR_UNPACKED, TEX_ADDR);
		//copyTexAddrUnpacked(FILLERS, TEX_ADDR);
		generateHMaxes(HMAX, HMAX_OFFSETS);
		packHMaxes(HMAX, 0x1e00);
		prefetcherBanksAndPages(SCR_OFFSETS, SCR_BANKS);
		//unpackTexture(TEX_DATA_PK, TEX_DATA);
		unpackTextureAsm();
		copySkyData(SKYLINE);
		prefetcherWithBanks(); //uses TEX_ADDR
		generateScreenFillersAsm(FILLERS, FILL_VEC);
		//testGenerateScreenFillers(FILLERS, FILL_VEC);
		//generateScreenFillers(FILLERS, FILL_VEC);
		//generateColFillers(COLFILL, COLFILL_VEC);
		generateColFillersAsm(COLFILL, COLFILL_VEC);
		//generateColFillersTest(COLFILL, COLFILL_VEC);
		//generateCBases(CBASES);
		generateCBasesTest(CBASES);
		//generateHMaxes(HMAX, HMAX_OFFSETS);
		//generatePrefetch(PREFETCH, TEX_ADDR, PIX_DATA); //10kb
				
		//setup HMAX offsets high
		asm.lda_imm(HMAX_PG);
		asm.sta(HMAX_H);
		
		//setup col fillers vectors high
		asm.lda_imm(COLFILL_VPG);
		asm.sta(CF_H);
		
		asm.lda_imm(0);
		asm.sta(SEAFR);
		
		//drawSkyOnce();
		drawSkyOnceAsm();
	}
	

	private void drawSkyOnce() {
		int memAddr = this.SCR_MEM;
		int colPix;
		for (int i=0;i<skycols.length;i++) {
			colPix = skycols[i]-0x90;
			colPix = colPix<<4;
			colPix += skycols[i]-0x90;
			for (int r=0;r<40;r++) {
				asm.mem.bytes[SCR_MEM+i*40+r]=colPix;
			}
		}
	}
	
	private void drawSkyOnceAsm() {
		asm.lda_imm(Asm.getLower(SCR_MEM));
		asm.sta(FILL_L);
		//System.out.println("SkyOne FILL_L:"+asm.mem.bytes[FILL_L]);
		asm.lda_imm(Asm.getUpper(SCR_MEM));
		asm.sta(FILL_H);
		//System.out.println("SkyOne FILL_H:"+asm.mem.bytes[FILL_H]);
		asm.ldx_imm(0);					
		Loop rowLoop = new Loop();
		while (rowLoop.flag) {
			asm.lda_imm(0);
			asm.sta(COLUMN);
			//System.out.println("Row:"+asm.cpu.x);
			asm.ldy_imm(0);
			//load value to be used
			//asm.lda_abs(SKYLINE,"x");
			//System.out.println("Skyone row:"+asm.cpu.x+" and color:"+asm.cpu.acu);
			//now iterate through the screen
			Loop cLoop = new Loop();
			while (cLoop.flag) {	
				asm.lda_abs(SKYLINE,"x");
				asm.sta_in(FILL_L,"y");
				//System.out.println("SkyOne in loop y:"+asm.cpu.y);
				jsrIncreaseFillAddr();
				//System.out.print("#");
				asm.lda_imm(40);
				asm.isb(COLUMN);
				if (asm.beq()) cLoop.flag=false;
			}
			//System.out.println("");
			asm.inx();
			asm.cpx_imm(110);
			if (asm.beq()) rowLoop.flag=false;
		}
	}

	private void copySkyData(int skyoffset) {
		int colPix;
		for (int i=0;i<skycols.length;i++) {
			colPix = skycols[i]-0x90;
			colPix = colPix<<4;
			colPix += skycols[i]-0x90;
			for (int r=0;r<40;r++) {
				asm.mem.bytes[skyoffset+i]=colPix;
			}
		}
		
	}
	
	private void drawSkyLine(int col, int row) {
		for (int i=SCR_MEM+row*40;i<SCR_MEM+row*40+40;i++) {			
				asm.mem.bytes[i]=col;
		}
	}
	
	private void drawSkyLineAsm() {
		//assume right address is in FILL_L/FILL_H
		//assume col amount is in ACU
		Loop rLoop = new Loop();
		asm.ldy_imm(40);
		while (rLoop.flag) {
			asm.sta_in(FILL_L, "y");
			asm.dey();
			if (asm.beq()) rLoop.flag=false;
		}
	}

	private void generateCBasesTest(int addr) {
		for (int i=0;i<24;i++) {
			if (asm.mem.bytes[addr+i]!=i*4) Asm.asmFailed("CBASES failed for i:"+i, i*4, asm.mem.bytes[addr+i]);
		}
	}

	private void generateCBases(int addr) {
		for (int i=0;i<24;i++) {
			asm.mem.bytes[addr+i]=i*4;
		}
	}
	
	private void unpackTexture(int texpacked, int textarget) {
		int pixLeft;
		int pixRight;
		for (int i=0;i<64*32;i++) {
			pixLeft = asm.mem.bytes[texpacked+i];
			pixLeft = pixLeft & 0b11110000;
			pixLeft = pixLeft >>> 4;
			pixRight = asm.mem.bytes[texpacked+i];
			pixRight = pixRight & 0b00001111;
			asm.mem.bytes[textarget+2*i]=pixLeft;
			asm.mem.bytes[textarget+2*i+1]=pixRight;
		}
	}
	
	private void unpackTextureAsm() {
		asm.lda_imm(TEX_DATA_PKPG);
		asm.sta(FILL_H);
		asm.lda_imm(TEX_DATA_PG);
		asm.sta(SF_H);
		asm.lda_imm(0);
		asm.sta(FILL_L);
		asm.sta(SF_L);
		asm.ldy_imm(0);
		Loop unpackLoop = new Loop();
		while (unpackLoop.flag) {
			asm.lda_in(FILL_L, "y");
			//backup temporarly to x
			asm.tax();
			//left pixel
			asm.and_imm(0b11110000);
			asm.lsr();
			asm.lsr();
			asm.lsr();
			asm.lsr();
			//store left
			asm.sta_in(SF_L, "y");
			jsrIncreaseTargetAddr();
			asm.txa();
			asm.and_imm(0b00001111);
			asm.sta_in(SF_L, "y");
			jsrIncreaseTargetAddr();
			jsrIncreaseFillAddr();
			//asm.lda_imm(TEX_DATA_PKPG_END); //base texture
			asm.lda_imm(0x1e); // with hmax page
			asm.cmp(FILL_H);
			if (asm.beq()) {
				//asm.lda_imm(0);
				asm.lda_imm(0x70); // with hmax page
				asm.cmp(FILL_L);
				if (asm.beq()) unpackLoop.flag=false;
			}
		}
	}
	
	private void generateHMaxes(int hmax_addr, int vectors_addr) {
		//row (from bottom) -> max height
		//0 - 15
		//1,2 - 14
		//3 - 13
		//4,5 - 12
		//6 - 11
		//7,8 - 10
		//9 - 9
		//10,11 - 8
		//12 - 7
		//13,14 - 6
		//15 - 5
		//16,17 - 4 
		//18 - 3
		//19,20 - 2
		//21,22 - 1
		//formula - new_h = acu_h*r_max/16; acu_h = 0 - 15
		int[] maxes = new int[] {15,14,14,13,12,12,11,10,10, 9, 8, 8, 7, 6, 6, 5, 4, 4, 3, 2, 2, 1, 1,0,0};
		int[] vectors= new int[]{ 0, 1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9, 9,10,11,11,12,13,13,14,14,14,14};
		for (int i=0;i<vectors.length;i++) {
			//asm.mem.bytes[vectors_addr+i]=vectors[i]*16;
			for (int h=0;h<16;h++) {
				asm.mem.bytes[hmax_addr+vectors[i]*16+h]=(h*maxes[i])>>4;				
			}
		}
		//now offsets
		for (int i=0;i<vectors.length;i++) {
			asm.mem.bytes[vectors_addr+i]=vectors[i]*16;
		}
		for (int i=0;i<16;i++) asm.mem.bytes[IDENTITY+i]=i;
	}
	
	private void packHMaxes(int hmax_addr, int target_addr)	
	{
		int left;
		int tindex=0;
		for (int i=0;i<256;i+=2) {
			left = asm.mem.bytes[hmax_addr+i];
			left = left << 4;
			left +=asm.mem.bytes[hmax_addr+i+1];
			asm.mem.bytes[target_addr+tindex]=left;
			tindex++;
		}		
	}
	
	private void generateColFillersAsm(int fill_addr, int vectors_addr) {
		asm.ldy_imm(24);
		asm.ldx_imm(0);
		Loop rowLoop = new Loop();
		while (rowLoop.flag) {
			asm.lda_imm(Asm.STA_ZPX);
			asm.sta_abs(this.COLFILL, "x");
			asm.inx();
			asm.lda_imm(0x9f);
			asm.sta_abs(this.COLFILL, "x");
			asm.inx();
			asm.lda_imm(Asm.INX);
			asm.sta_abs(COLFILL, "x");
			asm.inx();
			asm.dey();
			if (asm.beq()) rowLoop.flag=false;
		}
		asm.lda_imm(Asm.JMP_ABS);
		asm.sta_abs(COLFILL, "x");
		asm.inx();
		asm.lda_imm(0x6e);
		asm.sta_abs(COLFILL, "x");
		asm.inx();
		asm.lda_imm(0x10);
		asm.sta_abs(COLFILL, "x");
		asm.inx();
		//now vectors
		rowLoop.flag=true;
		asm.ldy_imm(24);
		asm.lda_imm(3);
		while (rowLoop.flag) {
			asm.sta_abs(this.COLFILL_VEC-1,"y");
			asm.adc_imm(3);
			asm.dey();
			if (asm.beq()) rowLoop.flag=false; 
		}
		//55 bytes + JSR + RTS -> 59 bytes
		//vs. 24*3 + 3 + 24 => 100 bytes
		
		//generate CBASEs
		asm.ldy_imm(24);
		asm.lda_imm(96);
		Loop cloop = new Loop();
		while (cloop.flag) {
			asm.sta_abs(CBASES, "y");
			//System.out.println("CBASES for:"+asm.cpu.y+"="+asm.cpu.acu);
			asm.sbc_imm(4);
			asm.dey();
			if (asm.beq()) cloop.flag=false;			
		}	
		// 12 vs 24b
	}
	
	private void generateColFillersTest(int fill_addr, int vectors_addr) {
		//fill up to 24 pixels, but most not used probably
		int caddr=fill_addr;
		int vaddr = vectors_addr;
		//for (int i=0;i<30;i++) {
		for (int i=0;i<24;i++) {
			if (asm.mem.bytes[caddr++]!=Asm.STA_ZPX) Asm.asmFailed("STA ZPX failed for:"+i);
			if (asm.mem.bytes[caddr++]!=0x9f) Asm.asmFailed("COLVAL failed for:"+i);;
			if (asm.mem.bytes[caddr++]!=Asm.INX) Asm.asmFailed("INX failed for:"+i);;
		}		
		//finish with jump
		if (asm.mem.bytes[caddr++]!=Asm.JMP_ABS) Asm.asmFailed("Colfill JMP failed");
		//asm.mem.bytes[caddr++]=0x57; //trigger jump back,
		//asm.mem.bytes[caddr++]=0x5a; //trigger jump back,
		//asm.mem.bytes[caddr++]=0x6a; //trigger jump back,
		if (asm.mem.bytes[caddr++]!=0x6e) Asm.asmFailed("back address low failed"); //trigger jump back,
		if (asm.mem.bytes[caddr++]!=0x10) Asm.asmFailed("back address high failed");; //trigger jump back,
		//jmp back to $1057
		//fill offsets indexes
		//for (int i=0;i<30;i++) {
		for (int i=0;i<24;i++) {		
			//0 -> 24*3 (so jmp)
			//1 -> 23*3 (so 1 + jmp)
			if (asm.mem.bytes[vaddr++]!=24*3-i*3) Asm.asmFailed("Wrong collfill vector for i:"+i, 24*3-i*3, asm.mem.bytes[vaddr-1]);
		}
	}
	
	private void generateColFillers(int fill_addr, int vectors_addr) {
		//fill up to 24 pixels, but most not used probably
		int caddr=fill_addr;
		int vaddr = vectors_addr;
		//for (int i=0;i<30;i++) {
		for (int i=0;i<24;i++) {
			asm.mem.bytes[caddr++]=Asm.STA_ZPX;
			asm.mem.bytes[caddr++]=0x9f;
			asm.mem.bytes[caddr++]=Asm.INX;
		}		
		//finish with jump
		asm.mem.bytes[caddr++]=Asm.JMP_ABS;
		//asm.mem.bytes[caddr++]=0x57; //trigger jump back,
		//asm.mem.bytes[caddr++]=0x5a; //trigger jump back,
		//asm.mem.bytes[caddr++]=0x6a; //trigger jump back,
		asm.mem.bytes[caddr++]=0x6e; //trigger jump back,
		asm.mem.bytes[caddr++]=0x10; //trigger jump back,
		//jmp back to $1057
		//fill offsets indexes
		//for (int i=0;i<30;i++) {
		for (int i=0;i<24;i++) {		
			//0 -> 24*3 (so jmp)
			//1 -> 23*3 (so 1 + jmp)
			asm.mem.bytes[vaddr++]=24*3-i*3;
		}
	}
	
	private void generateScreenFillersAsm(int addr, int vaddr) {
		//iterate target_addr (+1, word)
		//iterate offset_addr (+2, word) <- target addr
		//iterate screen addr (-40, word)
		//iterate source addr (COLVAL+1, 1 byte)
		//iterate column id (0->39)
		//iterate row id (0->91)

		asm.lda_imm(FILLERS_PG);
		asm.sta(FILL_H);
		asm.lda_imm(0);
		asm.sta(FILL_L);
		asm.sta(COLUMN);
		asm.lda_imm(Asm.getLower(asm.antic.getScreenPointer()+188*40));
		asm.sta(SCRS_L);
		asm.lda_imm(Asm.getUpper(asm.antic.getScreenPointer()+188*40));
		asm.sta(SCRS_H);
		Loop cLoop = new Loop();
		while (cLoop.flag) {
			//setup vector for a given column
			
			asm.lda(COLUMN);
			asm.asl();
			asm.tay();
			asm.lda(FILL_L);
			asm.sta_abs(FILL_VEC, "y");
			asm.lda(FILL_H);
			asm.iny();
			asm.sta_abs(FILL_VEC, "y");			
			asm.lda_imm(0);
			asm.sta(ROW);
			asm.ldx_imm(COLVAL);		
			
			asm.lda(SCRS_L);
			asm.sta(SF_L);
			asm.lda(SCRS_H);
			asm.sta(SF_H);
			Loop rLoop = new Loop();
			while (rLoop.flag) {
				//5 bytes per row
				asm.ldy_imm(0);
				asm.lda_imm(Asm.LDA_ZP); 
				asm.sta_in(FILL_L, "y");
				asm.iny();
				asm.txa();
				asm.sta_in(FILL_L, "y");
				asm.iny();
				asm.lda_imm(Asm.STA_ABS);
				asm.sta_in(FILL_L,"y");
				asm.iny();
				//now screen address
				asm.lda(SF_L);
				asm.sta_in(FILL_L, "y");
				asm.iny();
				asm.lda(SF_H);
				asm.sta_in(FILL_L, "y");
				jsrIncreaseFillAddr();				
				jsrIncreaseFillAddr();
				jsrIncreaseFillAddr();
				jsrIncreaseFillAddr();
				jsrIncreaseFillAddr();
				jsrDecreaseScreenAddr(); //decrease screen addres (-40)
				if (asm.mem.bytes[COLUMN]==0 && asm.mem.bytes[ROW]<10) System.out.println("Screen address:"+asm.mem.bytes[SF_L]+","+asm.mem.bytes[SF_H]);
				asm.inx(); //increase COLVAL+row
				//asm.lda_imm(92);
				asm.lda_imm(84);
				asm.isb(ROW);
				if (asm.beq()) rLoop.flag=false;
			}
			//if (asm.mem.bytes[ROW]!=92) Asm.asmFailed("Total rows incorrect for c="+asm.mem.bytes[COLUMN],92, asm.mem.bytes[ROW]);
			//additional 4 bytes for NOP (padding) and jmp back
			asm.ldy_imm(0);
			asm.lda_imm(Asm.NOP);
			asm.sta_in(FILL_L, "y");
			asm.iny();
			asm.lda_imm(Asm.JMP_ABS);
			asm.sta_in(FILL_L, "y");
			asm.iny();
			asm.lda_imm(0x71);
			asm.sta_in(FILL_L, "y");
			asm.iny();
			asm.lda_imm(0x10);
			asm.sta_in(FILL_L, "y");
			jsrIncreaseFillAddr();
			jsrIncreaseFillAddr();
			jsrIncreaseFillAddr();			
			jsrIncreaseFillAddr();
			//increase screen address (to next column)
			asm.inc(SCRS_L);
			if (asm.beq()) asm.inc(SCRS_H);
			asm.lda_imm(40);
			asm.isb(COLUMN);
			if (asm.beq()) cLoop.flag=false;
		}
	}
	
	private void jsrIncreaseFillAddr() {
		asm.clc();
		asm.inc(FILL_L);
		if (asm.beq()) {
			//System.out.println("Increased FILL_H to:"+asm.mem.bytes[FILL_H]);
			asm.inc(FILL_H);
		}
	}

	private void jsrIncreaseTargetAddr() {
		asm.clc();
		asm.inc(SF_L);
		if (asm.beq()) {
			//System.out.println("Increased FILL_H to:"+asm.mem.bytes[FILL_H]);
			asm.inc(SF_H);
		}
	}


	private void jsrDecreaseScreenAddr() {
		asm.clc();
		//System.out.println("Before decrease:"+asm.mem.bytes[SF_L]);
		asm.lda(SF_L);
		asm.sec();
		asm.sbc_imm(40); //go row back		
		if (asm.bcc()) {
			asm.dec(SF_H);
			//System.out.println("Decreased SF_H to:"+asm.mem.bytes[SF_H]);
		}
		asm.sta(SF_L);
		//System.out.println("After decrease:"+asm.mem.bytes[SF_L]);
	}

	
 
	private void generateScreenFillersTest(int addr, int vaddr) {
		
		//testing method to validate asm refactored method
		int caddr=addr;
		for (int c=0;c<40;c++) {
			//setup vector
			if (asm.mem.bytes[vaddr+c*2]!=Asm.getLower(caddr)) Asm.asmFailed("Lower vector not correct for:"+c,Asm.getLower(caddr),asm.mem.bytes[vaddr+c*2]);
			if (asm.mem.bytes[vaddr+c*2+1]!=Asm.getUpper(caddr)) Asm.asmFailed("Upper vector not correct for:"+c,Asm.getUpper(caddr),asm.mem.bytes[vaddr+c*2+1]);
			//asm.antic.getScreenPointer()+96*40+23*4*40+i
			int saddr = asm.antic.getScreenPointer()+188*40+c;
			for (int r=0;r<92;r++) {
				//if (r!=90 && r!=88 && r!=86 && r!=84 && r!=80 && r!=77 && r!=74) {
				//if (r%2==0) {
				//for each row, from the bottom
				//lda COLVAL+r
				//if (r%4==0) {
					if (asm.mem.bytes[caddr++]!=Asm.LDA_ZP) Asm.asmFailed("Asm LDA failed! for col:"+c+" and row:"+r,Asm.LDA_ZP, asm.mem.bytes[caddr-1]); //2b
					if (asm.mem.bytes[caddr++]!=COLVAL+r) Asm.asmFailed("COLVAL+r failed for col:"+c+" and row:"+r, COLVAL+r, asm.mem.bytes[caddr-1]);
				//}
				//sta SCRPOINTER
				if (asm.mem.bytes[caddr++]!=Asm.STA_ABS) Asm.asmFailed("STA_ABS store failed"); //3b
				if (asm.mem.bytes[caddr++]!=Asm.getLower(saddr)) Asm.asmFailed("STA_ABS lower byte store failed for c:"+c+" r:"+r,Asm.getLower(saddr), asm.mem.bytes[caddr-1] );
				if (asm.mem.bytes[caddr++]!=Asm.getUpper(saddr)) Asm.asmFailed("STA_ABS upper byte store failed for c:"+c+" r:"+r,Asm.getUpper(saddr), asm.mem.bytes[caddr-1]);					

				//}
				saddr-=40;
				// 0-91 -> 92 * 5 =  460
			}
			// NOP for padding
			if (asm.mem.bytes[caddr++]!=Asm.NOP) Asm.asmFailed("Failed putting NOP for c:"+c); //trigger jump back,
			if (asm.mem.bytes[caddr++]!=Asm.JMP_ABS) Asm.asmFailed("Failed putting JMP_ABS for c:"+c ); //trigger jump back,
			if (asm.mem.bytes[caddr++]!=0x71) Asm.asmFailed("Failed putting 0x71 return H for c:"+c); //trigger jump back,
			if (asm.mem.bytes[caddr++]!=0x10) Asm.asmFailed("Failed putting 0x10 return H for c:"+c); //trigger jump back,
			// +3
			// ret back to $1071
			//!!!! this neeeds to be padded to an even value of bytes in real, otherwise it will crash
		}
	}
	
	private void generateScreenFillers(int addr, int vaddr) {
		int caddr=addr;
		for (int c=0;c<40;c++) {
			//setup vector
			asm.mem.bytes[vaddr+c*2]=Asm.getLower(caddr);
			asm.mem.bytes[vaddr+c*2+1]=Asm.getUpper(caddr);
			//asm.antic.getScreenPointer()+96*40+23*4*40+i
			int saddr = asm.antic.getScreenPointer()+188*40+c;
			for (int r=0;r<92;r++) {
				//if (r!=90 && r!=88 && r!=86 && r!=84 && r!=80 && r!=77 && r!=74) {
				//if (r%2==0) {
				//for each row, from the bottom
				//lda COLVAL+r
				//if (r%4==0) {
					asm.mem.bytes[caddr++]=Asm.LDA_ZP; //2b
					asm.mem.bytes[caddr++]=COLVAL+r;
				//}
				//sta SCRPOINTER
				asm.mem.bytes[caddr++]=Asm.STA_ABS; //3b
				asm.mem.bytes[caddr++]=Asm.getLower(saddr);
				asm.mem.bytes[caddr++]=Asm.getUpper(saddr);
				//}
				saddr-=40;
				// 0-91 -> 92 * 5 =  460
			}
			// NOP for padding
			asm.mem.bytes[caddr++]=Asm.NOP; //trigger jump back,
			asm.mem.bytes[caddr++]=Asm.JMP_ABS; //trigger jump back,
			asm.mem.bytes[caddr++]=0x71; //trigger jump back,
			asm.mem.bytes[caddr++]=0x10; //trigger jump back,
			// +3
			// ret back to $1071
			//!!!! this neeeds to be padded to an even value of bytes in real, otherwise it will crash
		}
	}

	private void packTexAddr(int source, int target) {
		HashMap<Integer, Integer> pageMap = new HashMap<>();
		//first find all upper bytes
		int valIndex=0;
		for (int i=source+1; i<source + 80*24*2;i+=2) {
			if (!pageMap.containsKey(asm.mem.bytes[i])) {
				pageMap.put(asm.mem.bytes[i], valIndex);
				valIndex++;
			}
		}
			
		System.out.println("Upper byte values:");
		//Set<Integer> keys = pageMap.keySet();
		//Integer[] keysInt = keys.toArray(new Integer[0]);
		for (Integer key:pageMap.keySet()) {
			System.out.println("Upper byte:"+String.format("%02X", key)+":"+pageMap.get(key));
		}
		//now pack it
		int targetIndex = target;
		//lower bytes first
		for (int i=source; i<source + 80*24*2;i+=2) {
			asm.mem.bytes[targetIndex++]=asm.mem.bytes[i];
		}
		//upper bytes pairs
		for (int i=source; i<source + 80*24*2;i+=4) {
			int packedValue=0;
			packedValue = pageMap.get(asm.mem.bytes[i+1]);
			packedValue = packedValue<<4;
			packedValue += pageMap.get(asm.mem.bytes[i+3]);			
			asm.mem.bytes[targetIndex++]=packedValue;
		}			
		//now clean the old addr area
		for (int i=TEX_ADDR;i<TEX_ADDR+80*24*2;i++) asm.mem.bytes[i]=0x00;
	}
	
	private void copyTexAddrPacked(int source, int target) {
		for (int i=0;i<80*24+40*24;i++) {
			asm.mem.bytes[target+i]=asm.mem.bytes[source+i];
		}
	}
	
	private void copyTexAddrUnpacked(int source, int target)
	{
		for (int i=0;i<80*24*2;i++) {
			asm.mem.bytes[target+i]=asm.mem.bytes[source+i];
		}
	}

	private void unpackTexAddrAsm() {
		
		int[] byteVals = new int[] {0x9d,0x9e,0x9f,0xa1,0xa2,0xa4,0xa6,0xa8,0xab,0xa5,0xa9,0xa7,0x0};
		
		//FILL_L contains source		
		//asm.lda_imm(Asm.getLower(TEX_ADDR_PACKED));
		asm.lda_imm(Asm.getLower(TEX_ADDR));
		asm.sta(FILL_L);
		//asm.lda_imm(Asm.getUpper(TEX_ADDR_PACKED));
		asm.lda_imm(Asm.getUpper(TEX_ADDR));
		asm.sta(FILL_H);
		//SF_L contains target
		asm.lda_imm(Asm.getLower(TEX_ADDR_UNPACKED));
		asm.sta(SF_L);
		asm.lda_imm(Asm.getUpper(TEX_ADDR_UNPACKED));
		asm.sta(SF_H);
		//unpack the lower bytes part
		asm.ldy_imm(0);
		Loop lowbytes = new Loop();
		int lowercount=0;
		while (lowbytes.flag) {
			asm.lda_in(FILL_L, "y");
			asm.sta_in(SF_L, "y");
			jsrIncreaseFillAddr(); //source address
			jsrIncreaseTargetAddr();
			jsrIncreaseTargetAddr();
			//asm.lda_imm(Asm.getUpper(TEX_ADDR_PACKED+80*24));
			asm.lda_imm(Asm.getUpper(TEX_ADDR+80*24));
			asm.cmp(FILL_H);
			if (asm.beq()) {
				//asm.lda_imm(Asm.getLower(TEX_ADDR_PACKED+80*24));
				asm.lda_imm(Asm.getLower(TEX_ADDR+80*24));
				asm.cmp(FILL_L);
				if (asm.beq()) lowbytes.flag=false;
			}
			lowercount++;
		}
		//now the upper bytes
		//keep the source address as it looks fine from the lower bytes run
		//set just the target address
		asm.lda_imm(Asm.getLower(TEX_ADDR_UNPACKED));
		asm.sta(SF_L);
		asm.lda_imm(Asm.getUpper(TEX_ADDR_UNPACKED));
		asm.sta(SF_H);
		//increase it by one, to set to high bytes
		jsrIncreaseTargetAddr();
		//now unpack the upper bytes
		Loop upperloop = new Loop();
		int uppercount=0;
		while (upperloop.flag) {
			//one packed translates to two unpacked bytes
			asm.lda_in(FILL_L, "y");
			asm.tax(); //make a copy
			asm.and_imm(0b11110000);
			asm.lsr();
			asm.lsr();
			asm.lsr();
			asm.lsr();
			//now fetch page from the table
			asm.tay();
			asm.lda_imm(byteVals[asm.cpu.y]); //this will be from the array eventually
			asm.ldy_imm(0);
			asm.sta_in(SF_L, "y");
			jsrIncreaseTargetAddr();
			jsrIncreaseTargetAddr();
			//now do the other byte
			asm.txa(); // copy the value back from x reg
			asm.and_imm(0b00001111);
			asm.tay();
			/*
			if (asm.cpu.y>12) {
				System.out.println("Something unpacked wrongly:");
				System.out.println("Lower/Uppercount:"+lowercount+"/"+uppercount);
				System.out.println("Packed offset:"+Asm.printHexValByte(asm.mem.bytes[FILL_L])+","+Asm.printHexValByte(asm.mem.bytes[FILL_H]));
				System.out.println("Packed value (2 halves):"+Asm.printHexValByte(asm.cpu.x));
			}
			*/
			asm.lda_imm(byteVals[asm.cpu.y]); //this will be from the array eventually
			asm.ldy_imm(0);
			asm.sta_in(SF_L, "y");
			jsrIncreaseTargetAddr();
			jsrIncreaseTargetAddr();
			jsrIncreaseFillAddr();
			//should end at $CB40
			//asm.lda_imm(Asm.getUpper(TEX_ADDR_PACKED+80*24+40*24));
			asm.lda_imm(Asm.getUpper(TEX_ADDR+80*24+40*24));
			asm.cmp(FILL_H);
			if (asm.beq()) {
				//asm.lda_imm(Asm.getLower(TEX_ADDR_PACKED+80*24+40*24));
				asm.lda_imm(Asm.getLower(TEX_ADDR+80*24+40*24));
				asm.cmp(FILL_L);
				if (asm.beq()) upperloop.flag=false;
			}
			//if (asm.mem.bytes[FILL_H]==203)
			//	System.out.println("Packed offset:"+Asm.printHexValByte(asm.mem.bytes[FILL_L])+","+Asm.printHexValByte(asm.mem.bytes[FILL_H]));
			uppercount++;
		}
	}

	private void unpackTexAddr(int packed, int unpacked) {
				
		int[] byteVals = new int[] {0x9d,0x9e,0x9f,0xa1,0xa2,0xa4,0xa6,0xa8,0xab,0xa5,0xa9,0xa7,0x0};
		
		int unpackedAddr = unpacked;
		int packedAddr= packed;
		//unpack the lower bytes part
		for (int i=0;i<80*24;i++) {
			asm.mem.bytes[unpackedAddr]=asm.mem.bytes[packedAddr];
			unpackedAddr++;
			unpackedAddr++;
			packedAddr++;
		}
		unpackedAddr=unpacked;
		unpackedAddr++;
		//unpack the upper bytes
		for (int i=0;i<40*24;i++) {
			int packedVal = asm.mem.bytes[packedAddr];
			int unpackedValLeft=packedVal & 0b11110000;
			unpackedValLeft = unpackedValLeft>>>4;
			unpackedValLeft = byteVals[unpackedValLeft];
			asm.mem.bytes[unpackedAddr]=unpackedValLeft;
			unpackedAddr++;
			unpackedAddr++;
			unpackedValLeft = packedVal & 0b00001111;
			unpackedValLeft = byteVals[unpackedValLeft];
			asm.mem.bytes[unpackedAddr]=unpackedValLeft;
			unpackedAddr++;
			unpackedAddr++;
			packedAddr++;
		}
	}
	
	private void updateColorsAsmZigzag() {
		Set<Integer> upperByteSet = new HashSet<Integer>();
		
		int camDistance=100;
		//texture2D = SimAntic.getpApplet().loadImage("terrain2.png");
		int zVal;
		int xVal;
		int zTexture;
		int prevZTexture=0;
		int xTexture;
		int textVal;
		PApplet p = SimAntic.getpApplet();
		int texAddrOffset = TEX_ADDR;
		int texDataOffset;
		for (int x=0;x<40;x+=2) {
			for (int y=47;y>=25;y--) {
				zVal = (screenPoints[y][0].y*camDistance)/(1*(camera.y-screenPoints[y][0].y));			
				zTexture = (zVal+zOffset)%textureSize;
				//left pixel
				xVal = (40-x)*(camDistance+zVal)/camDistance;				
				xTexture = (xVal-32-xOffset)%textureSize;
				if (xTexture<0) xTexture*=-1;
				//System.out.print(xTexture+" ");
				//screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));
				texDataOffset = TEX_DATA + zTexture*textureSize+xTexture;
				if (texDataOffset>TEX_DATA+4096) System.out.println("Offset out of tex range!");
				asm.lda_imm(Asm.getLower(texDataOffset));
				//upperByteSet.add(asm.cpu.acu);
				asm.sta(texAddrOffset);
				texAddrOffset++;
				asm.lda_imm(Asm.getUpper(texDataOffset));
				upperByteSet.add(asm.cpu.acu);
				asm.sta(texAddrOffset);
				texAddrOffset++;
				//right pixel
				xVal = (40-x-1)*(camDistance+zVal)/camDistance;				
				xTexture = (xVal-32-xOffset)%textureSize;
				if (xTexture<0) xTexture*=-1;
				//System.out.print(xTexture+" ");
				//screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));
				texDataOffset = TEX_DATA + zTexture*textureSize+xTexture;
				asm.lda_imm(Asm.getLower(texDataOffset));
				asm.sta(texAddrOffset);
				texAddrOffset++;
				asm.lda_imm(Asm.getUpper(texDataOffset));
				asm.sta(texAddrOffset);
				texAddrOffset++;
			}
		}
		for (int x=40;x<80;x+=2) {
			for (int y=47;y>=25;y--) {
				zVal = (screenPoints[y][0].y*camDistance)/(1*(camera.y-screenPoints[y][0].y));			
				zTexture = (zVal+zOffset)%textureSize;
				//left pixel
				xVal = (x-40)*(camDistance+zVal)/camDistance;
				System.out.print(xVal+" ");
				xTexture = (32+xVal+xOffset)%textureSize;
				//System.out.print(xTexture+" ");
				//screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));
				texDataOffset = TEX_DATA + zTexture*textureSize+xTexture;
				if (texDataOffset>TEX_DATA+4096) System.out.println("Offset out of tex range!");
				asm.lda_imm(Asm.getLower(texDataOffset));
				asm.sta(texAddrOffset);
				//upperByteSet.add(asm.cpu.acu);
				texAddrOffset++;
				asm.lda_imm(Asm.getUpper(texDataOffset));
				upperByteSet.add(asm.cpu.acu);
				asm.sta(texAddrOffset);
				texAddrOffset++;
				//right pixel
				xVal = (x+1-40)*(camDistance+zVal)/camDistance;
				System.out.print(xVal+" ");
				xTexture = (32+xVal+xOffset)%textureSize;
				//System.out.print(xTexture+" ");
				//screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));
				texDataOffset = TEX_DATA + zTexture*textureSize+xTexture;
				asm.lda_imm(Asm.getLower(texDataOffset));
				asm.sta(texAddrOffset);
				texAddrOffset++;
				asm.lda_imm(Asm.getUpper(texDataOffset));
				asm.sta(texAddrOffset);
				texAddrOffset++;
			}
		}

		for (int y=47;y>=25;y--) {
			zVal = (screenPoints[y][0].y*camDistance)/(1*(camera.y-screenPoints[y][0].y));			
			hmax[y]=camDistance*(24-8)/(camDistance+zVal);
			asm.ldy_imm(y-25);
			asm.lda_imm((int)camDistance*(24-8)/(camDistance+zVal));
			asm.sta_abs(HMAX, "y");
		}
		for (int y=25;y<48;y++) {
		System.out.println("Hmax["+y+"]:"+(int)hmax[y]);
		}

		System.out.println("Upper byte count:"+upperByteSet.size());
		System.out.print("Upper bytes:");
		for (Integer integer:upperByteSet) {
			System.out.print(String.format("%02X", integer)+",");
		}
		System.out.println("");
	}
	
	private void updateColors() {
		int camDistance=100;

		//texture2D = SimAntic.getpApplet().loadImage("terrain2.png");
		int zVal;
		int xVal;
		int zTexture;
		int prevZTexture=0;
		int xTexture;
		int textVal;
		PApplet p = SimAntic.getpApplet();
		for (int y=25;y<48;y++) {
			zVal = (screenPoints[y][0].y*camDistance)/(1*(camera.y-screenPoints[y][0].y));			
			zTexture = (zVal+zOffset)%textureSize;
			//System.out.println("z("+y+"):"+zTexture);
			//zTexture = (zVal)%textureSize;
			//System.out.println("X offsets for Y:"+y);
			for (int x=0;x<40;x++) {
				xVal = (40-x)*(camDistance+zVal)/camDistance;				
				xTexture = (xVal-32-xOffset)%textureSize;
				if (xTexture<0) xTexture*=-1;
				//System.out.print(xTexture+" ");
				screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));				
			}
			//System.out.println("");			
			for (int x=40;x<80;x++) {
				xVal = (x-40)*(camDistance+zVal)/camDistance;
				//System.out.print(xVal+" ");
				xTexture = (32+xVal+xOffset)%textureSize;
				//System.out.print(xTexture+" ");
				screenColors[y][x]=(int)p.red(texture2D.get(xTexture, zTexture));
			}
			//System.out.println();

			//System.out.println("");
			hmax[y]=camDistance*(24-8)/(camDistance+zVal);
		}
		for (int y=25;y<48;y++) {
		//System.out.println("Hmax["+y+"]:"+(int)hmax[y]);
		}
		
	}
	
	@Override
	public void drawHelperView(PApplet p) {
		drawHelperOpt2(p);
	}

	@Override
	public int getHelperWidth() {
		return 600;
	}
	@Override
	public int getHelperHeight() {
		return 240;
	}
	
	public void drawHelperOpt2(PApplet p) { //column by column
		p.noFill();
		p.stroke(255);
		p.rect(0, 0, 80*4, 48*4);
	
		int offsetX=0;
		int offsetY=0;

		updateColors();
		p.noStroke();
	 
		// OK!
		for (int x=0;x<80;x++)
		{
			int cMax = 0; //starting max col height
			int cBase=0;
			int cHeight=0;
			int color;
			for (int y=47;y>25;y--) {
				//from bottom to up			
				color= screenColors[y][x];
				cHeight = (int)(1*(hmax[y]*color/255.0f));
				if (cHeight+cBase>cMax) {
					//fill column with colors
					ColorRGB seaCol = GtiaRGB.gtia2RGB(9*16+color/16);
					p.fill(seaCol.red, seaCol.green, seaCol.blue);
					p.rect(offsetX+x*4, offsetY+y*4-cHeight, 4, (cHeight+cBase-cMax));
					cMax = cHeight + cBase;
				}
				cBase+=4;
			}
		}
		
		frameCount++;
		if (frameCount%5==0) {
			zOffset++;
			
		}
		//draw column historgra
		p.fill(255);
		p.stroke(150);
		p.textSize(8);
		p.text("Fill column height histogram:", offsetX+80*4+10, offsetY+10);
		//100 line
		p.line(offsetX+80*4+10, offsetY+30+100, offsetX+80*4+200, offsetY+30+100);
		//200 line
		p.line(offsetX+80*4+10, offsetY+30+200, offsetX+80*4+200, offsetY+30+200);
		//300 line
		p.line(offsetX+80*4+10, offsetY+30+300, offsetX+80*4+200, offsetY+30+300);
		p.noStroke();
		for (int i=0;i<colfills.length;i++) {
			p.text(i, offsetX+80*4+10+10*i, offsetY+20);
			p.rect(offsetX+80*4+10+10*i, offsetY+30, 6, colfills[i]);
		}
	}

	public void drawPlaneZigzag() {
		int texAddrOffset=TEX_ADDR;
		int screenPtr=asm.antic.getScreenPointer()+96*40+23*40; //start with the beginning of last line
		for (int i=0;i<40;i++) {
			screenPtr=asm.antic.getScreenPointer()+96*40+23*4*40+i;
			for (int y=0;y<23;y++) {
				asm.ldy_imm(0);
				asm.lda_in(texAddrOffset, "y");
				//shift color (so we get 16 colors)
				//asm.lsr(); asm.lsr(); asm.lsr(); asm.lsr();
				//store in left nibble (outside asm for now)
				asm.asl(); asm.asl(); asm.asl(); asm.asl();
				asm.sta(PIX_VAL); //temporarily
				texAddrOffset+=2;			
				asm.lda_in(texAddrOffset, "y");
				//asm.lsr(); asm.lsr(); asm.lsr(); asm.lsr();
				asm.ora(PIX_VAL);
				asm.ldy_imm(0);
				asm.sta_abs(screenPtr,"y");				
				asm.ldy_imm(-40);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(-80);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(-120);
				asm.sta_abs(screenPtr,"y");
				texAddrOffset+=2;
				screenPtr-=120;
			}			
		}		
	}

	public void drawPlane() {
		int texAddrOffset=TEX_ADDR;
		int screenPtr=asm.antic.getScreenPointer()+96*40;		
		for (int y=0;y<23;y++) {
			for (int i=0;i<40;i++) { 
				asm.ldy_imm(0);
				asm.lda_in(texAddrOffset, "y");
				//shift color (so we get 16 colors)
				//asm.lsr(); asm.lsr(); asm.lsr(); asm.lsr();
				//store in left nibble (outside asm for now)
				asm.asl(); asm.asl(); asm.asl(); asm.asl();
				asm.sta(PIX_VAL); //temporarily
				texAddrOffset+=2;			
				asm.lda_in(texAddrOffset, "y");
				//asm.lsr(); asm.lsr(); asm.lsr(); asm.lsr();
				asm.ora(PIX_VAL);
				asm.ldy_imm(0);
				asm.sta_abs(screenPtr,"y");				
				asm.ldy_imm(40);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(80);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(120);
				asm.sta_abs(screenPtr,"y");
				texAddrOffset+=2;
				screenPtr++;
			}
			screenPtr+=120;
		}
		
	}
		
	public void drawVoxelsBase() {
		int texAddrOffset=TEX_ADDR;
		int hCurr;
		int screenPtr=asm.antic.getScreenPointer()+96*40;
		int color;
		for (int y=0;y<23;y++) {
			for (int i=0;i<40;i++) {
				//left pixel
				asm.ldy_imm(0); // 2b
				asm.lda_in(texAddrOffset, "y"); //3b
				asm.pha(); // keep the color for height calculation 1b
				//store in left nibble (outside asm for now)
				//asm.sta(PIX_VAL); // single pix
				asm.asl(); asm.asl(); asm.asl(); asm.asl(); //4b
				//asm.ora(PIX_VAL); // single pix
				asm.sta(PIX_VAL); //temporarily				
				asm.pla();
				//asm.cpu.acu=color;
				hCurr = (int)((asm.mem.bytes[HMAX+y]*asm.cpu.acu)>>4);				
				asm.ldy_imm(hCurr);
				asm.lda(PIX_VAL);
				asm.cpu.y+=4;
				for (int v=0;v<asm.cpu.y;v++) {
					asm.lda(screenPtr-v*40);
					asm.and_imm(0b00001111);
					asm.ora(PIX_VAL);
					asm.sta(screenPtr-v*40);
				}
				texAddrOffset+=2;
				
				//right pixel
				
				asm.ldy_imm(0);
				asm.lda_in(texAddrOffset, "y");				
				asm.pha(); // keep the color for height calculation
				asm.sta(PIX_VAL); //temporarily
				asm.pla();
				hCurr = (int)((asm.mem.bytes[HMAX+y]*asm.cpu.acu)>>4);				
				asm.ldy_imm(hCurr);
				asm.cpu.y+=4;

				for (int v=0;v<asm.cpu.y;v++) {
					asm.lda(screenPtr-v*40);
					asm.and_imm(0b11110000);
					asm.ora(PIX_VAL);
					asm.sta(screenPtr-v*40);
				}								
				texAddrOffset+=2;
				screenPtr++;
			}
			/*
			screenPtr-=40;
			for (int i=0;i<40;i++) {
				asm.ldy_imm(0);
				asm.lda_abs(screenPtr,"y");
				asm.ldy_imm(40);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(80);
				asm.sta_abs(screenPtr,"y");
				asm.ldy_imm(120);
				asm.sta_abs(screenPtr,"y");
				screenPtr++;
			}*/
			screenPtr+=120;			
		}
		
	}

	public void drawVoxelsOpt6() { //call by call, texture prefetcher, cbase precalced, colfillers unrolled, height precalced
		
		colfills = new int[24];
		asm.profSectionStart(" voxels");
		asm.lda_imm(0x00); 
		asm.sta(TEX_L);			
		asm.lda_imm(PIX_PG);
		asm.sta(TEX_H);
		
		asm.lda_imm(0);
		asm.sta(COLUMN);
		
		Loop colLoop = new Loop();
		colLoop.flag=true;		
		while (colLoop.flag) {			
		//for (int i=0;i<40;i++) {
			//40 times
			asm.lda_imm(0); //2b
			asm.sta(CMAX);  //2b		
			asm.sta(ROW);	//2b
			//for each column (2column)
			Loop rowloop= new Loop();
			rowloop.flag=true;
			while(rowloop.flag) {
				//40x23 times
				//with unroll - -24c each iteration, so 23k savings?? but seems not possible :/
				asm.profSectionStart("  rowiter");
				asm.ldy_imm(0); //2c			
				asm.lda_in(TEX_L,"y"); //5c
				asm.sta(PIX_VAL); //3c
				asm.inc(TEX_L); //5c
				if (asm.beq()) asm.inc(TEX_H); //6c
				asm.and_imm(0b00001111); //2c
				//use right as height;
				asm.tay(); // have the height in y, for indirect fetch 1b //2c
				asm.ldx(ROW);				//3c
				asm.lda_abs(HMAX_OFFSETS, "x"); //4c
				asm.sta(HMAX_L); //3c
				//now load height based on PIX_RIGHT
				asm.lda_in(HMAX_L,"y"); //5c
				//a hack until we have a table, but we want to simulate the cycles for now...
				asm.adc(CBASES,"x"); //4c		
				asm.cmp(CMAX); //2 //3c
				asm.profSectionEnd("  rowiter");
				asm.profSectionStart("  column");
				if (asm.bcs()) //1
				{
					asm.profSectionStart("   coliter");					
					//we need to know until when to store
					asm.sbc(CMAX); //3c
					//colfills[asm.cpu.acu]++; //debug only
					asm.tay(); //2c
					asm.lda_abs(COLFILL_VEC,"y"); //4c
					asm.sta(CF_L); //3c
					asm.lda(PIX_VAL); //3c
					asm.ldx(CMAX); //3c
					asm.profSectionEnd("   coliter");
					asm.profSectionStart("   innercol");
					asm.jmp_in(CF_L);
					//this.protoColFiller(asm.cpu.y);
					asm.profSectionEnd("   innercol");
					asm.profSectionStart("   coliter");
					asm.stx(CMAX); //3c
					asm.profSectionEnd("   coliter");
				}
				else colfills[0]++; //debug only
				asm.profSectionEnd("  column");
				asm.profSectionStart("  rowiter");
				asm.lda_imm(23); //2c
				asm.isb(ROW); //5c 
				if (asm.beq()) rowloop.flag=false; //3c
				asm.profSectionEnd("  rowiter");				
			}
			asm.profSectionStart("  colfiller");
			asm.lda(COLUMN);
			asm.asl();
			asm.tay();
			asm.lda_abs(FILL_VEC, "y");
			asm.sta(SF_L);
			asm.iny();
			asm.lda_abs(FILL_VEC, "y");
			asm.sta(SF_H);
			asm.jmp_in(SF_L);
			asm.profSectionEnd("  colfiller");
			asm.lda_imm(40);
			asm.isb(COLUMN);
			if (asm.beq()) colLoop.flag=false;
		}		
		asm.profSectionEnd(" voxels");				
	}	

public void drawVoxelsOpt7() { // with banks and initial prefetcher
		
		colfills = new int[24];
		asm.profSectionStart(" voxels");
		asm.lda_imm(0x00); 
		asm.sta(TEX_L);			
		//asm.lda_imm(PIX_PG);
		asm.ldy(SEAFR);
		asm.lda_abs(SCR_OFFSETS, "y");		
		asm.sta(TEX_H);
		
		asm.lda_abs(SCR_BANKS, "y");		
		asm.sta(CBANK);
		//System.out.println("Bank for seaframe:"+asm.mem.bytes[SEAFR]+" is:"+asm.mem.bytes[CBANK]+" page is:"+asm.mem.bytes[TEX_H]);
		asm.lda_imm(0);
		asm.sta(COLUMN);
		
		Loop colLoop = new Loop();
		colLoop.flag=true;		
		while (colLoop.flag) {			
		//for (int i=0;i<40;i++) {
			//40 times
			//now switch bank
			//asm.switchBank(true, asm.mem.bytes[CBANK]);
			asm.lda(CBANK);
			asm.sta(MMap.PORTB);
			
			asm.lda_imm(0); //2b
			asm.sta(CMAX);  //2b		
			asm.sta(ROW);	//2b
			//for each column (2column)
			Loop rowloop= new Loop();
			rowloop.flag=true;
			while(rowloop.flag) {
				//40x23 times
				//with unroll - -24c each iteration, so 23k savings?? but seems not possible :/
				asm.profSectionStart("  rowiter");
				asm.ldy_imm(0); //2c			
				asm.lda_in(TEX_L,"y"); //5c
				asm.sta(PIX_VAL); //3c
				asm.inc(TEX_L); //5c
				if (asm.beq()) asm.inc(TEX_H); //6c
				asm.and_imm(0b00001111); //2c
				//use right as height;
				asm.tay(); // have the height in y, for indirect fetch 1b //2c
				asm.ldx(ROW);				//3c
				asm.lda_abs(HMAX_OFFSETS, "x"); //4c
				asm.sta(HMAX_L); //3c
				//now load height based on PIX_RIGHT
				asm.lda_in(HMAX_L,"y"); //5c
				//a hack until we have a table, but we want to simulate the cycles for now...
				asm.adc(CBASES,"x"); //4c		
				asm.cmp(CMAX); //2 //3c
				asm.profSectionEnd("  rowiter");
				asm.profSectionStart("  column");
				if (asm.bcs()) //1
				{
					asm.profSectionStart("   coliter");					
					//we need to know until when to store
					asm.sbc(CMAX); //3c
					colfills[asm.cpu.acu]++; //debug only
					asm.tay(); //2c
					asm.lda_abs(COLFILL_VEC,"y"); //4c
					asm.sta(CF_L); //3c
					asm.lda(PIX_VAL); //3c
					asm.ldx(CMAX); //3c
					asm.profSectionEnd("   coliter");
					asm.profSectionStart("   innercol");
					asm.jmp_in(CF_L);
					//this.protoColFiller(asm.cpu.y);
					asm.profSectionEnd("   innercol");
					asm.profSectionStart("   coliter");
					asm.stx(CMAX); //3c
					asm.profSectionEnd("   coliter");
				}
				else colfills[0]++; //debug only
				asm.profSectionEnd("  column");
				asm.profSectionStart("  rowiter");
				asm.lda_imm(23); //2c
				asm.isb(ROW); //5c 
				if (asm.beq()) rowloop.flag=false; //3c
				asm.profSectionEnd("  rowiter");				
			}
			asm.profSectionStart("  colfiller");
			//switch back the bank to colfiller (RAM)
			//asm.switchBank(false, 0);
			asm.lda_imm(0b11111111);
			asm.sta(MMap.PORTB);
			asm.lda(COLUMN);
			asm.asl();
			asm.tay();
			asm.lda_abs(FILL_VEC, "y");
			asm.sta(SF_L);			
			asm.iny();
			asm.lda_abs(FILL_VEC, "y");
			asm.sta(SF_H);
			//System.out.println("Fill vector :"+asm.mem.bytes[SF_H]+","+asm.mem.bytes[SF_L]);
			asm.jmp_in(SF_L);
			asm.profSectionEnd("  colfiller");
			asm.lda_imm(40);
			asm.isb(COLUMN);
			if (asm.beq()) colLoop.flag=false;
		}		
		asm.profSectionEnd(" voxels");				
	}	
	
	public void copyFromTextureToPixelsOraOptAbs(int sourcevectors, int destdata) {
		//this will be unrolled
		asm.profSectionStart("prefetch");
		asm.lda_imm(0);
		asm.sta(COLVAL+0);
		asm.lda_imm(16);
		asm.sta(COLVAL+1);
		asm.lda_imm(32);
		asm.sta(COLVAL+2);
		asm.lda_imm(48);
		asm.sta(COLVAL+3);
		asm.lda_imm(64);
		asm.sta(COLVAL+4);
		asm.lda_imm(80);
		asm.sta(COLVAL+5);
		asm.lda_imm(96);
		asm.sta(COLVAL+6);
		asm.lda_imm(112);
		asm.sta(COLVAL+7);
		asm.lda_imm(128);
		asm.sta(COLVAL+8);
		asm.lda_imm(144);
		asm.sta(COLVAL+9);
		asm.lda_imm(160);
		asm.sta(COLVAL+10);
		asm.lda_imm(176);
		asm.sta(COLVAL+11);
		asm.lda_imm(192);
		asm.sta(COLVAL+12);
		asm.lda_imm(208);
		asm.sta(COLVAL+13);
		asm.lda_imm(224);
		asm.sta(COLVAL+14);
		asm.lda_imm(240);
		asm.sta(COLVAL+15);
								
		asm.ldy_imm(0);
		for (int i=0;i<40*24;i++) {
			//asm.lda_in(sourcevectors+i*4, "y");
			asm.ldx(asm.mem.peek_word(sourcevectors+i*4)); ////nonexistent, but simulates absolute mode
			asm.lda_abs(COLVAL, "x");
			asm.ora(asm.mem.peek_word(sourcevectors+i*4+2));
			asm.sta(destdata+i);
		}
		//drop originals vector table (source vectors)
		asm.profSectionEnd("prefetch");
	}
	
	public void prefetcherBanksAndPages(int pages_vec, int banks_vec) {
		// bits: 
		   // 7 - RAM select 1 - on
		   // 6 - not used
           // 5 - ANTIC select (0 - ext, 1 main)
           // 4 - CPU select
           // 3 & 2  bank select
           // 1 - BASIC Enable - 0 - on
           // 0 - OS ROM enable - 0 - off
		   // bank RAM range - $4000 - $7FFF
	           // 3-2: 00 - bank 0, 10 - bank 1, 01 - bank 2, 11 - bank 3
		   // 0-bank additional : 0b11100011
		int bankSelected=0;
		for (int f=0;f<64;f++) {
			if (f<16) bankSelected=0b11100011;
			if (f>=16 && f<32) bankSelected=0b11101011;
			if (f>=32 && f<48) bankSelected=0b11100111;
			if (f>=48 && f<64) bankSelected=0b11101111;
			//asm.lda_imm()
		}
		//bank 0
		asm.lda_imm(0b11100011);
		for (int i=0;i<16;i++) { asm.sta(SCR_BANKS+i);}
		//bank 1
		asm.lda_imm(0b11101011);
		for (int i=0;i<16;i++) { asm.sta(SCR_BANKS+16+i);}
		//bank 2
		asm.lda_imm(0b11100111);
		for (int i=0;i<16;i++) { asm.sta(SCR_BANKS+32+i);}
		//bank 3
		asm.lda_imm(0b11101111);
		for (int i=0;i<16;i++) { asm.sta(SCR_BANKS+48+i);}
		//and now pages
		//bank 0
		asm.lda_imm(0x40);
		asm.clc();
		for (int i=0;i<16;i++) {
			asm.sta(SCR_OFFSETS+i);
			asm.sta(SCR_OFFSETS+16+i);
			asm.sta(SCR_OFFSETS+32+i);
			asm.sta(SCR_OFFSETS+48+i);
			asm.adc_imm(4);
		}
	}
	
	public void prefetcherWithBanks() {
		//inputs:
		//  raw texture data
		//  texture vectors (zigzag form)
		//outputs:
		//  screen pixels (in linear form, ready to consume) - single frame: 40x24
		//  screen offsets and bank mask
		//for 64 frames:

		//  load pixels from texture
		//  store them in the right bank (in address range $4000-7fff)
		//  generate offset and bank mask
		//  move the sea
		//int bankSelected=0; //original RAM
		Loop frameLoop = new Loop();
		asm.lda_imm(0);
		asm.sta(SEAFR);
		//for (int f=0;f<64;f++) {
		int frameindex=0;
		while(frameLoop.flag) {
			//asm.ldy_imm(f);
			asm.ldy(SEAFR);
			//asm.sta_abs(SCR_BANKS, "y");
			asm.lda_abs(SCR_BANKS, "y");
			asm.sta(MMap.PORTB);
			// select right page (just and the last 4 bits and add $40)
			//asm.tya();
			//asm.and_imm(0b00001111); //now multiple x4, as we need 4 pages for each frame
			//asm.asl();
			//asm.asl();
			//asm.adc_imm(0x40);  // as the bank starts from page $40
			//asm.sta_abs(SCR_OFFSETS,"y");
			asm.lda_abs(SCR_OFFSETS, "y");
			asm.ldy_imm(0);
			//init sf_l and sf_h for indirect store
			asm.sta(SF_H);			
			asm.lda_imm(0);
			asm.sta(SF_L);
			//asm.lda_imm(0x80); //due to shift original, unpacked
			asm.lda_imm(0x00); //packed
			asm.sta(FILL_L); //use fill location for indirect index through TEX_ADDR
			//asm.lda_imm(TEX_ADDR_PG); // original, unpacked
			//asm.lda_imm(0xe0); //packed
			asm.lda_imm(Asm.getUpper(TEX_ADDR_UNPACKED)); //packed
			asm.sta(FILL_H);
			Loop pixLoop = new Loop();
			int pixindex=0;
			while (pixLoop.flag) {
				//System.out.println("frame:"+frameindex+" pixindex:"+pixindex);				
			//for (int i=0;i<40*24;i++) {
				//if (sourcevectors+i*4>TEX_DATA+4096) {
				//	System.out.println("Exception in fetch:"+i+" at sourcevectors:"+sourcevectors);
				//	System.exit(0);
				//}
				//asm.lda_in(sourcevectors+i*4, "y");
				asm.lda_in(FILL_L, "y");				
				asm.sta(CF_L);
				asm.iny();
				asm.lda_in(FILL_L, "y");
				asm.sta(CF_H);
				if (pixindex<10 && frameindex<5) {
					System.out.println("Loaded indirectly CF_H:"+ asm.cpu.acu+"from FILL_L:"+asm.printHexWord(FILL_L));
				}
				asm.dey();
				asm.lda_in(CF_L, "y");
				//asm.lda_in(FILL_L, "y");
				if (pixindex<10 && frameindex<5) {
					System.out.println("Fetching pixel 1 from:"+asm.printHexWord(CF_L));
				}
				jsrIncreaseFillAddr(); //FILL_L+1
				jsrIncreaseFillAddr(); //FILL_L+1
				asm.asl();
				asm.asl();
				asm.asl();
				asm.asl();
				asm.tax(); //copy it temporarily

				asm.lda_in(FILL_L, "y");
				if (asm.cpu.acu>255) {
					System.out.println("Trying to fetch bad pixel offset from:"+
							String.format("%02X", asm.mem.bytes[FILL_H])+","+String.format("%02X", asm.mem.bytes[FILL_L]));
				}
				asm.sta(CF_L);
				asm.iny();				
				asm.lda_in(FILL_L, "y");
				if (asm.cpu.acu>255) {
					System.out.println("Trying to fetch bad pixel offset from:"+
							String.format("%02X", asm.mem.bytes[FILL_H])+","+String.format("%02X", asm.mem.bytes[FILL_L]));
				}
				
				asm.sta(CF_H);
				asm.txa(); //copy back
				//asm.ora_in(sourcevectors+i*4+2,"y");
				asm.dey();
				//System.out.println("Fetching pixel 2 from:"+asm.printHexWord(CF_L));
				asm.ora_in(CF_L,"y");
				
				asm.sta_in(SF_L, "y");
				asm.inc(SF_L);
				if (asm.beq()) asm.inc(SF_H);
				jsrIncreaseFillAddr(); //FILL_L+1
				jsrIncreaseFillAddr(); //FILL_L+1
				//if FILL_L/FILL_H = $2580, end
				
				
				//asm.lda_imm(0x2D); //original
				// tex_addr_unpacked
				asm.lda_imm(Asm.getUpper(TEX_ADDR_UNPACKED+80*24*2));
				//asm.lda_imm(Asm.getUpper(FILLERS+80*24*2));
				//asm.lda_imm(0x66);
				//asm.lda_imm(Asm.getUpper(0xee80));
				asm.cmp(FILL_H);
				if (asm.beq()) {
					//asm.lda_imm(0x00); //original
					asm.lda_imm(Asm.getLower(TEX_ADDR_UNPACKED+80*24*2));
					//asm.lda_imm(Asm.getLower(0xee80));
					//asm.lda_imm(Asm.getLower(FILLERS+80*24*2));
					asm.cmp(FILL_L);
					if (asm.beq()) pixLoop.flag=false;
				}
				pixindex++;
			}
			//System.out.println("Frame fetch ended at:"+asm.printHexWord(FILL_L));
			//System.exit(0); //debug only
			System.out.println("Prefetch frame:"+frameindex);
			moveSeaAsm();
			frameindex++;
			asm.lda_imm(65);
			asm.isb(SEAFR);
			if (asm.beq()) frameLoop.flag=false;
		}
		//asm.switchBank(false, 0);
		asm.lda_imm(0b11111111);
		asm.sta(MMap.PORTB);
		
	}
	
	public void generatePrefetch(int prefetchAddr, int sourcevectors, int destdata) {
		//this will be unrolled
		int caddr=prefetchAddr;
		for (int i=0;i<16;i++) {
			//asm.lda_imm(i*16);
			asm.mem.bytes[caddr++]=Asm.LDA_IMM;
			asm.mem.bytes[caddr++]=i*16;
			//asm.sta(COLVAL,i); //this is to  be changed eventually
			asm.mem.bytes[caddr++]=Asm.STA_ZP;
			asm.mem.bytes[caddr++]=COLVAL+i;
		}
		
				
		//asm.ldy_imm(0);
		for (int i=0;i<40*24;i++) {
			//asm.ldx(asm.mem.peek_word(sourcevectors+i*4)); ////nonexistent, but simulates absolute mode
			asm.mem.bytes[caddr++]=Asm.LDX_ABS;
			asm.mem.bytes[caddr++]=Asm.getLower(asm.mem.peek_word(sourcevectors+i*4));
			asm.mem.bytes[caddr++]=Asm.getUpper(asm.mem.peek_word(sourcevectors+i*4));			
			//asm.lda_abs(COLVAL, "x");
			asm.mem.bytes[caddr++]=Asm.LDA_ZPX;
			asm.mem.bytes[caddr++]=COLVAL;			
			//asm.ora(asm.mem.peek_word(sourcevectors+i*4+2));
			asm.mem.bytes[caddr++]=Asm.ORA_ABS;
			asm.mem.bytes[caddr++]=Asm.getLower(asm.mem.peek_word(sourcevectors+i*4+2));
			asm.mem.bytes[caddr++]=Asm.getUpper(asm.mem.peek_word(sourcevectors+i*4+2));
			//asm.sta(destdata+i);
			asm.mem.bytes[caddr++]=Asm.STA_ABS;
			asm.mem.bytes[caddr++]=Asm.getLower(destdata+i);
			asm.mem.bytes[caddr++]=Asm.getUpper(destdata+i);
		}
		//asm.mem.bytes[caddr]=Asm.JMP_ABS; //ending jump
		asm.mem.bytes[caddr]=Asm.RTS; //ending jump
		
		//drop originals vector table (source vectors)	
		for (int i=0;i<80*24*2;i++) asm.mem.bytes[sourcevectors+i]=0;
	}
	

	
	@Override
	public void stepFx() {
		// set some pattern on the screen...
		
		asm.resetCount();
		//cleanScreen();
		//drawPlane();
		//drawPlaneZigzag();
		//drawVoxelsOpt1();
		//copyFromTextureToPixels(TEX_ADDR, PIX_DATA);		
		//copyFromTextureToPixelsOraOptAbs(TEX_ADDR, PIX_DATA);
		//drawVoxelsOpt6(); // with texture prefetcher, pure asm, all unrolled
		drawVoxelsOpt7(); // with banks and initial prefetcher
		
		drawSkyLine(0x66, 105);
		drawSkyLine(0x88, 106);
		drawSkyLine(0x77, 108);
		drawSkyLine(0x66, 110);
		
		if (asm.mem.peek(MMap.RTCLOCK_3)%5==0) {
			//moveSeaAsm();
			//increase seaframe
			asm.inc(SEAFR);
			if (asm.mem.bytes[SEAFR]==64) {
				asm.lda_imm(0);
				asm.sta(SEAFR);
			}
			//asm.mem.dumpMemoryToFile("sea"+dumpIndex+".dat", 0x3e00, 64*64);
			//asm.mem.dumpMemoryToHexFile(SimAntic.getpApplet(), "seahex"+dumpIndex+".dat", 0x3e00, 64*64, 64);
			dumpIndex++;
		}		

	}
	
	private void moveSea() {
		int[] buffer = new int[64];
		//move the sea closer to the viewer
		//copy the last line
		
		//OK!
		//for (int i=0;i<64;i++) {
		//	asm.mem.bytes[CPY_BUFFER+i]=asm.mem.bytes[TEX_DATA+i];
		//	//buffer[i]=asm.mem.bytes[TEX_DATA+i];
		//}
		
		//asm.profSectionStart("seamove");
		asm.ldy_imm(64);		
		Loop loop = new Loop();
		while(loop.flag) {			
			asm.lda_abs(TEX_DATA-1, "y");
			asm.sta_abs(CPY_BUFFER-1,"y");
			asm.dey();
			asm.bne(loop);
		}
	
		for (int i=0;i<63*64;i++) {
			asm.lda(TEX_DATA+i+64);
			asm.sta(TEX_DATA+i);			
			//asm.mem.bytes[TEX_DATA+i]= asm.mem.bytes[TEX_DATA+i+64];
		}
		
		asm.ldy_imm(64);		
		loop = new Loop();
		while(loop.flag) {
			asm.lda_abs(CPY_BUFFER-1,"y");
			asm.sta_abs(TEX_DATA+63*64-1, "y");
			asm.dey();
			asm.bne(loop);
		}
	}
	
	private void moveSeaAsm() {
		
		//asm.profSectionStart("seamove");		
		asm.ldy_imm(64);		
		Loop loop = new Loop();
		while(loop.flag) {			
			asm.lda_abs(TEX_DATA-1, "y");
			asm.sta_abs(COLVAL-1,"y");
			asm.dey();
			asm.bne(loop);
		}
			
		asm.lda_imm(TEX_DATA_PG);
		asm.sta(TO_H);
		asm.sta(FROM_H);
		asm.lda_imm(0);
		asm.sta(TO_L);
		asm.clc();
		asm.adc_imm(64);
		asm.sta(FROM_L);


		asm.ldx_imm(64);
		Loop xloop = new Loop();
		//System.out.println("Copy loop-----------------------------------------------------------");
		while(xloop.flag) {				
		//for (int i=0;i<64;i++) {
			asm.ldy_imm(64);
			loop = new Loop();
			while(loop.flag) {			
				asm.lda_in(FROM_L, "y");
				asm.sta_in(TO_L, "y");
				asm.dey();
				asm.bne(loop);
			}
			//copy last pixel
			//asm.lda_in(FROM_L, "y");
			//asm.sta_in(TO_L, "y");			
			//increase from
			asm.lda(FROM_L);
			asm.clc();
			asm.adc_imm(64);
			if (asm.bcs()) asm.inc(FROM_H);
			asm.sta(FROM_L);
			asm.lda(TO_L);
			asm.clc();
			asm.adc_imm(64);
			if (asm.bcs()) asm.inc(TO_H);
			asm.sta(TO_L);
			//System.out.println("Sea copy:"+asm.mem.bytes[FROM_L]+","+asm.mem.bytes[FROM_H]+"->"+asm.mem.bytes[TO_L]+","+asm.mem.bytes[TO_H]);
			asm.dex();
			asm.bne(xloop);
		}		
			
		//copy temp buffer back to the end of texture
		asm.ldy_imm(64);		
		loop = new Loop();
		while(loop.flag) {
			asm.lda_abs(COLVAL-1,"y");
			asm.sta_abs(TEX_DATA+63*64-1, "y");
			asm.dey();
			asm.bne(loop);
		}

	}


	@Override
	public Memory getMemory() {
		return asm.mem;
	}
	
	public void cleanE000() {
		for (int i=0xe000;i<0xefff;i++)
			asm.mem.bytes[i]=0;
	}

}
