package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Antic.OsMode;

public class TestFxTextColor extends FxSimAbs {

	private int[] charsetData = new int[] {			
			0,0,0,0,0,0,0,0,
			68,0,17,0,68,0,17,0,
			68,17,0,68,17,0,68,17,
			68,17,85,68,17,85,68,17,
	      	17,85,68,85,17,85,68,85,
	      	85,85,85,85,85,85,85,85,
	      	153,85,102,85,153,85,102,85,
	      	153,102,85,153,102,85,153,102,
	      	153,102,170,153,102,170,153,102,
	      	102,170,153,170,102,170,153,170,
	      	170,170,170,170,170,170,170,170,
	      	238,170,187,170,238,170,187,170,
	      	238,187,170,238,187,170,238,187,
	      	238,187,255,238,187,255,238,187,
	      	238,255,187,255,238,255,187,255,
	      	255,255,255,255,255,255,255,255};

	@Override
	public String getFxName() {		
		return "TestFx Textmode (color)";
	}

	public TestFxTextColor() {
		super();
		offsets.add("ScreenMem:0xbba0");
		initFx();
	}
	@Override
	public void initFxSim() {
		//load rom
		asm.mem.loadRom();
	}

	@Override
	public void initFx() {
		// setup color
		asm.antic.setMode(OsMode.TEXT4); //16 scales of backcolor

		//copy original charset
		for (int i=0;i<4*256;i++) {
			asm.lda(0xe000+i);
			asm.sta(0x7000+i);
		}
		//copy dither chars
		for (int i=0;i<charsetData.length;i++) {
			asm.lda_imm(charsetData[i]);
			asm.sta(0x7000+i);
		}
		//now set chbas
		asm.lda_imm(0x70);
		asm.sta(MMap.CHBAS);

	}

	@Override
	public void stepFx() {

		for (int i=0;i<16;i++) {
			asm.lda_imm(i);
			asm.sta(asm.antic.getScreenPointer()+i);
		}
			
		// set some pattern on the screen...		
		asm.lda_imm(0x32); // R
		asm.sta(asm.antic.getScreenPointer()+40);
		asm.lda_imm(0x25); // E
		asm.sta(asm.antic.getScreenPointer()+41);		
		asm.lda_imm(0x21); // A
		asm.sta(asm.antic.getScreenPointer()+42);		
		asm.lda_imm(0x24); // D
		asm.sta(asm.antic.getScreenPointer()+43);		
		asm.lda_imm(0x39); // Y
		asm.sta(asm.antic.getScreenPointer()+44);
		if (asm.mem.bytes[MMap.RTCLOCK_3]==120) {
			//randomize all the colors
			asm.mem.bytes[MMap.COLOR0]=(int)(Math.random()*255);
			asm.mem.bytes[MMap.COLOR1]=(int)(Math.random()*255);
			asm.mem.bytes[MMap.COLOR2]=(int)(Math.random()*255);
			//force font reload
			asm.antic.forceFontReload();
		}
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
