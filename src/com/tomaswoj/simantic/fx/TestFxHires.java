package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Antic.OsMode;

public class TestFxHires extends FxSimAbs {

	@Override
	public String getFxName() {		
		return "TestFx HiRes";
	}

	public TestFxHires() {
		super();
		offsets.add("ScreenMem:0xa150");
		initFx();
	}
	@Override
	public void initFxSim() {
		//nothing special here
		
	}

	@Override
	public void initFx() {
		//load rom
		asm.mem.loadRom();
		// setup color
		asm.antic.setMode(OsMode.HIRES); //16 scales of backcolor
		//asm.antic.setMode(OsMode.GTIA11); //16 hues of the backcolor luminance
		asm.lda_imm(0x80);
		asm.sta(MMap.COLOR2); // blue
		asm.lda_imm(0x08);
		asm.sta(MMap.COLOR1); // blue

	}

	@Override
	public void stepFx() {
		// set some pattern on the screen...
		
		for (int i=0;i<255;i++)
		{
			asm.lda_imm(0b10101010);
			asm.sta(asm.antic.getScreenPointer()+100+i);
		}
		
		for (int i=0;i<320;i++) {
			asm.antic.putpixel(i, (int)(100+30*Math.sin(i*3.14f/180)), 1);
		}
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

	@Override
	public int getHelperWidth() {
		return 100;
	}
	@Override
	public int getHelperHeight() {
		return 100;
	}

}
