package com.tomaswoj.simantic.fx;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import com.tomaswoj.simantic.asm.Asm;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;

import processing.core.PApplet;

public interface FxSim {
	public String getFxName();
	public void initFxSim(); // inits hw setup (cpu, mem, antic mode) needed for the fx
	public void initFx(); //inits the fx (precalcs if any)
	public void stepFx(); //single step (new frame) for the fx
	public void playWhole(); // multiple steps
	public void playCredits(int credits); // multiple steps
	public void playStep(); // multiple steps
	public void drawFx(PApplet p); // draw the fx
	public void drawHelperView(PApplet p);
	public int getHelperWidth();
	public int getHelperHeight();
	public Memory getMemory();
	public Cpu getCpu();
	public Antic getAntic();
	public Asm getAsm();
	public ArrayList<String> getOffsets(); 
	public void setShowStatus(boolean status);	
	public boolean isShowStatus();
	public void setDrawHelper(boolean helper);
	public Semaphore getSemaphore();	
}
