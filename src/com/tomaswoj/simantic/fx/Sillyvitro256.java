package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Antic.OsMode;

public class Sillyvitro256 extends FxSimAbs {

	private int shiftSize;
	private int offsetX; 

	//dithered color charset pattern
	private int[] charsetData = new int[] {			
			//0,0,0,0,0,0,0,0,
			68,0,17,0,68,0,17,0,
			68,17,0,68,17,0,68,17,
			68,17,85,68,17,85,68,17,
	      	17,85,68,85,17,85,68,85,
	      	85,85,85,85,85,85,85,85,
	      	153,85,102,85,153,85,102,85,
	      	153,102,85,153,102,85,153,102,
	      	};

	//sillyventure invitation text
	private int[] text = new int[] {0x23,0x2f,0x2d,0x25,0x00,0x34,0x2f,0x00,0x33,0x29,
									0x2c,0x2c,0x39,0x36,0x25,0x2e,0x34,0x35,0x32,0x25,
									0x00,0x12,0x2f,0x11,0x18,0x0c,0x00,0x27,0x24,0x21,
									0x2e,0x33,0x2b,0x00,0x12,0x0d,0x14,0x0e,0x11,0x11};	
	@Override
	public String getFxName() {		
		return "Sillyvitro 256b";
	}

	public Sillyvitro256() {
		super();
		offsets.add("ScreenMem:0xbba0");
		initFx();
	}
	
	@Override
	public void initFxSim() {
		//load rom, to get the original charset
		asm.mem.loadRom();	
	}

	@Override
	public void initFx() {
		// setup color
		asm.antic.setMode(OsMode.TEXT4);
		
		//copy original charset
		for (int i=0;i<2*256;i++) {
			asm.lda(0xe000+i);
			asm.sta(0x5400+i);
		}
		
		//copy dither chars
		for (int i=0;i<charsetData.length;i++) {
			asm.lda_imm(charsetData[i]);
			asm.sta(0x5408+i);
		}
		//now set new character base page
		asm.lda_imm(0x54);
		asm.sta(MMap.CHBAS);
		
		//now set colors:
		//$3a,$72,$38,$0e
		asm.lda_imm(0x72);
		asm.sta(MMap.COLOR0);
		asm.lda_imm(0x38);
		asm.sta(MMap.COLOR1);
		asm.lda_imm(0x0e);
		asm.sta(MMap.COLOR2);
		asm.lda_imm(0x00);
		asm.sta(MMap.COLOR3);
		
	}

	@Override
	public void stepFx() {
		asm.resetCount();
		// xor pattern effect
		for (int x=0;x<40;x++) {
			for (int y=0;y<23;y++) {
				int val = (x+offsetX)^(y+offsetX);
				val = val >>> shiftSize;
				val = val & 0b00000111;
				asm.lda_imm(val);
				asm.sta(asm.antic.getScreenPointer()+y*40+x);
			}			
		}
		
		// print text at the bottom of the screen
		for (int i=0;i<text.length;i++) {
			asm.mem.bytes[asm.antic.getScreenPointer()+40*23+i]=text[i];
		}
		
		if (asm.mem.bytes[MMap.RTCLOCK_3]%5==0) 
		{
			//every 5th frame move the offset
			offsetX++;
			if (offsetX>255) offsetX=0;
		}
				
		if (asm.mem.bytes[MMap.RTCLOCK_3]==120) {
			//randomize shiftSize
			shiftSize = (int)(Math.random()*4);
		}
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
