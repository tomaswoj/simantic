package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.SimAntic;
import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;
import com.tomaswoj.simantic.hw.Antic.OsMode;
import com.tomaswoj.simantic.utils.ImageUtils;

public class TestFxGtia9 extends FxSimAbs {

	@Override
	public String getFxName() {		
		return "GTIA9 with image load";
	}

	public TestFxGtia9() {
		super();
		offsets.add("ScreenMem:0xa150");
		initFx();
	}
	
	@Override
	public void initFxSim() {
		//nothing special here		
	}

	@Override
	public void initFx() {
		//load rom
		asm.mem.loadRom();
		// setup color
		asm.antic.setMode(OsMode.GTIA9); //16 scales of backcolor
		// load the image directly to the memory area, scale 255 grayscales to 16 shades, pack 2 pixels into 1 byte
		ImageUtils.loadImageToMemPacked(SimAntic.getpApplet(), "silly.png", asm.mem, asm.antic.getScreenPointer(), 0.0625f);
		asm.lda_imm(0x08);
		asm.sta(MMap.COLBACK); // grayscale
	}

	@Override
	public void stepFx() {
		asm.resetCount();
		// add some pattern on the top of the screen...		
		for (int i=0;i<255;i++)
		{
			asm.lda_imm(i);
			asm.sta(asm.antic.getScreenPointer()+100+i);
		}
		// randomize colors every ~4s
		if (asm.mem.peek(MMap.RTCLOCK_3)==0) {
			asm.lda_imm((int)(Math.random()*16));
			asm.sta(MMap.COLBACK);
		}
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
