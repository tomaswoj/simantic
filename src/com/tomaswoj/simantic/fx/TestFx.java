package com.tomaswoj.simantic.fx;

import com.tomaswoj.simantic.asm.Asm;
import com.tomaswoj.simantic.asm.MMap;
import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;

public class TestFx extends FxSimAbs {

	@Override
	public String getFxName() {		
		return "TestFx";
	}

	public TestFx() {
		super();
		initFx();
	}
	@Override
	public void initFxSim() {
		//nothing special here
		
	}

	@Override
	public void initFx() {
		asm.mem.loadRom();
		// setup mode
		asm.antic.setMode(Antic.OsMode.TEXT);
		// setup color
		//asm.lda_imm(0x8f);
		//asm.sta(MMap.COLBACK);
		asm.lda_imm(0x94);
		asm.sta(MMap.COLOR2); // blue
		asm.lda_imm(0xca);
		asm.sta(MMap.COLOR1); // blue
		offsets.add("ScreenMem:"+"0x"+Asm.printHexValWord(asm.antic.getScreenPointer()));

	}

	@Override
	public void stepFx() {
		// clean whole screen memory
		for (int i=0;i<80*24;i++) {
			asm.lda_imm(i);
			asm.tay();
			asm.lda_imm(0);
			//asm.sta_in(MMap.SAVMSC_L, "y");
		}
		// set one pixel on the screen...
		asm.lda_imm(64);
		//int clockoffset = asm.mem.peek(MMap.RTCLOCK_3);
		int clockoffset = 30;
		asm.sta(asm.antic.getScreenPointer()+clockoffset);
		//System.out.println("Put char "+ asm.cpu.acu+" at:"+Asm.printHexValWord(asm.antic.getScreenPointer()+clockoffset));
		//System.out.println("Char in mem:"+asm.mem.bytes[asm.antic.getScreenPointer()+clockoffset]);
	}

	@Override
	public Memory getMemory() {
		return asm.mem;
	}

}
