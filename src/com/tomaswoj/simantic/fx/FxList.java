package com.tomaswoj.simantic.fx;

import java.util.ArrayList;

public class FxList {

	private static ArrayList<FxSim> fxs;
	private static int currentFx=-1;
	
	public static FxSim getFx(int index) {
		FxSim result = null;
		if (index<fxs.size()) result = fxs.get(index);
		return result;
	}
	
	public static FxSim getFxByName(String name) {
		FxSim result = null;
		for (FxSim fxSim:fxs) {
			if (fxSim.getFxName().contains(name)) result = fxSim;
		}
		return result;
	}
	
	public static void addFx(FxSim fxSim) {
		if (fxs==null) fxs = new ArrayList<FxSim>();
		fxs.add(fxSim);
		if (currentFx==-1) currentFx=0; // set the default fx to 0, when first added
	}
	
	public int getCurrentFx() {
		return currentFx;
	}
	
	public ArrayList<String> getFxNames() {
		ArrayList<String> fxNames = new ArrayList<>();
		for (FxSim fxSim:fxs) fxNames.add(fxSim.getFxName());
		return fxNames;
	}
	
	public void setCurrentFx(int n) {
		this.currentFx=n;
	}
}
