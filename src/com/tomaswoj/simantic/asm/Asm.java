package com.tomaswoj.simantic.asm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.tomaswoj.simantic.hw.Antic;
import com.tomaswoj.simantic.hw.Cpu;
import com.tomaswoj.simantic.hw.Memory;

public class Asm {

	public static final int JMP_ABS=0x4c; //jmp $abs
	public static final int LDA_ZP=0xA5; //lda $zp
	public static final int LDA_IMM=0xA9; //lda imm
	public static final int LDA_ZPX=0xB5; // lda $zp,x
	public static final int LDX_ABS=0xAE; // ldx $abs
	public static final int STA_ABS=0x8D; // sta $abs
	public static final int STA_ZP=0x85; // sta $zp
	public static final int STA_ZPX=0x95; // sta $zp,x
	public static final int INX=0xe8;
	public static final int ORA_ABS=0x0D; // ora $abs
	public static final int NOP=0xEA; // just do nothing
	public static final int RTS=0x60; // just do nothing
	
	
	private LinkedHashMap<String, Integer> sectionCycles; //sectionCycles
	private ArrayList<String> sections; //current sections for profiling

	private int cycleCount=0;
	public Cpu cpu;
	public Memory mem;
	public Antic antic;

	public static Asm getAsm64k() {
		Asm asm = new Asm();
		asm.cpu = new Cpu();
		asm.mem = new Memory();
		asm.antic = new Antic();
		asm.antic.setMemory(asm.mem);
		return asm;
	}
	
	public void resetCount() {
		cycleCount=0;
		sectionCycles = new LinkedHashMap<>();
		sections = new ArrayList<>();
		sections.add("total");
		sectionCycles.put("total", 0);
	}
	
	private void pushCycles(int newCycles) {
		if (sections!=null)
		for (String cSection:sections) {
			sectionCycles.put(cSection,sectionCycles.get(cSection)+newCycles);
		}
	}
	
	public void profSectionStart(String section) {
		//add only if section does not exist
		boolean sectionExists=false;
		for (String cSection:sections) if (cSection.contains(section)) sectionExists=true;
		if (!sectionExists) {
			sections.add(section);
			if (sectionCycles.get(section)==null) sectionCycles.put(section, 0);
		}	
	}
	
	public void profSectionEnd(String section) {
		int sectionIndex=-1;
		for (int i=0;i<sections.size();i++) if (sections.get(i).contains(section)) sectionIndex=i;
		if (sectionIndex>-1) sections.remove(sectionIndex);		
	}
	
	public int getCount() {
		if (sectionCycles!=null && sectionCycles.containsKey("total")) {
			return sectionCycles.get("total");
		}
		else
			return -1;
		//return cycleCount;
	}
	
	public HashMap<String,Integer> getSectionCounts() {
		return sectionCycles;
	}
	
	public void iny() { 
			cpu.y++; 
			if (cpu.y>255) { cpu.y=0; cpu.zeroFlag=true;}
			else cpu.zeroFlag=false;
			//cycleCount+=2;
			pushCycles(2);
		}
	
	public void dey() { 
			cpu.y--; 
			if (cpu.y<0) cpu.y=255;
			if (cpu.y==0) cpu.zeroFlag=true;
			else cpu.zeroFlag=false;
			//cycleCount+=2;
			pushCycles(2);
		}
	
	public void inx() {
			cpu.x++; 
			if (cpu.x>255) { cpu.x=0; cpu.zeroFlag=true;}
			else cpu.zeroFlag=false;
			//cycleCount+=2;
			pushCycles(2);
		}
	
	public void dex() { 
			cpu.x--; 
			if (cpu.x<0) cpu.x=255;
			if (cpu.x==0) cpu.zeroFlag=true;
			else cpu.zeroFlag=false;
			//cycleCount+=2;
			pushCycles(2);
		}
	
	public boolean bne() {
		pushCycles(2);
		if (!cpu.zeroFlag) return true; 
		else return false;
	}
	
	public boolean bmi() {
		pushCycles(2);
		if (cpu.negativeFlag) return true;
		else return false;
	}
	
	public void bne(Loop loop) { 
		if (!cpu.zeroFlag) loop.flag=true;
		else loop.flag=false;
		//cycleCount+=3; //average if taken, 2 if not taken, 4 if taken + cross page boundary
		pushCycles(3);
	}
	
	public boolean bcs() {
		boolean result;
		if (cpu.carryFlag) result=true;
		else result=false;
		//cycleCount+=3; //average if taken
		pushCycles(3);
		return result;
	}

	public boolean bcc() {
		boolean result;
		if (!cpu.carryFlag) result=true;
		else result=false;
		//cycleCount+=3; //average if taken
		pushCycles(3);
		return result;
	}

	
	public boolean beq() {
		//cycleCount+=3; //average if taken, 2 if not taken, 4 if taken + cross page boundary
		pushCycles(3);
		if (cpu.zeroFlag) return true;
		else return false;

	}
	public void tax() {
		cpu.x=cpu.acu;
		if (cpu.x==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		pushCycles(2);
		//cycleCount+=2;
	}
	
	public void txa() {
		cpu.acu=cpu.x;
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void tay() {
		cpu.y=cpu.acu;
		if (cpu.y==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void tya() {
		cpu.acu=cpu.y;
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}			
	
	public void lda(int addr) {
		cpu.acu=mem.bytes[addr];
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (cpu.acu>127) cpu.negativeFlag=true;
		else cpu.negativeFlag=false;
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}
	
	public void lda_imm(int val) { 
		cpu.acu=val;
		if (val==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (cpu.acu>127) cpu.negativeFlag=true;
		else cpu.negativeFlag=false;

		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void lda_abs(int addr, String reg) {
		if (reg.contains("x")) { cpu.acu = mem.bytes[addr+cpu.x];}
		if (reg.contains("y")) { cpu.acu = mem.bytes[addr+cpu.y];}
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (cpu.acu>127) cpu.negativeFlag=true;
		else cpu.negativeFlag=false;

		//cycleCount+=4;
		pushCycles(4);
	}
	
	public void cmp(int addr) {
		if (cpu.acu>=mem.bytes[addr]) {
			cpu.carryFlag=true;		
		}
		if (cpu.acu==mem.bytes[addr]) {
			cpu.zeroFlag=true;
		}
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void cmp_imm(int val) {
		if (cpu.acu>=val) {
			cpu.carryFlag=true;		
		}
		if (cpu.acu==val) {
			cpu.zeroFlag=true;
		}		
		//cycleCount+=2;
		pushCycles(2);
	}

	public void cpx(int addr) {
		if (cpu.x>=mem.bytes[addr]) {
			cpu.carryFlag=true;		
		}
		if (cpu.x==mem.bytes[addr]) {
			cpu.zeroFlag=true;
		}
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void cpy_imm(int val) {
		if (cpu.y>=val) {
			cpu.carryFlag=true;		
		}
		if (cpu.y==val) {
			cpu.zeroFlag=true;
		}
		pushCycles(2);
	}

	public void cpx_imm(int val) {
		if (cpu.x>=val) {
			cpu.carryFlag=true;		
		}
		if (cpu.x==val) {
			cpu.zeroFlag=true;
		}
		pushCycles(2);
	}

	
	
	public void lda_in(int addr, String reg) {
		if (reg.contains("y")) {
			cpu.acu = mem.bytes[mem.peek_word(addr)+cpu.y];
		}
		if (reg.contains("x")) {
			cpu.acu = mem.bytes[mem.peek_word(addr+cpu.x)];
		}
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=6;
		//cycleCount+=5;
		pushCycles(5);
	}
	
	public void ldx_in(int addr) {
		//nonexistent, but simulates absolute mode
		cpu.x=mem.bytes[mem.peek_word(addr)];
		pushCycles(4);
	}
	
	public void sta(int addr) {
		mem.bytes[addr]=cpu.acu;
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
		memsetHandler(addr);
	}
	
	public void memsetHandler(int addr) {
		switch(addr) {
			case MMap.PORTB:
				handlePortB();
				break;
			default:
				break;
		}
	}
	
	public void handlePortB() {
		int value = mem.bytes[MMap.PORTB];
		switch(value) {
			case 0b11100011: //select 0bank ext
				//System.out.println("Switching to ext-bank 0");
				this.switchBank(true, 1);
				break;
			case 0b11101011: //select 1bank ext
				//System.out.println("Switching to ext-bank 1");
				this.switchBank(true, 2);
				break;
			case 0b11100111: //select 2bank ext
				//System.out.println("Switching to ext-bank 2");
				this.switchBank(true, 3);
				break;
			case 0b11101111: //select 3bank ext
				//System.out.println("Switching to ext-bank 3");
				this.switchBank(true, 4);
				break;
			case 0b11111111: //select default RAM
				//System.out.println("Switching to RAM 0");
				this.switchBank(false, 0);
				break;
			default:
				break;
		}
	}
	
	//non-existent but a good helper
	public void sta(int addr, int offset) {
		mem.bytes[addr+offset]=cpu.acu;
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void sta_abs(int addr, String reg) {
		if (reg.contains("x")) { mem.bytes[addr+cpu.x]=cpu.acu;}
		if (reg.contains("y")) { mem.bytes[addr+cpu.y]=cpu.acu;}
		if (addr<0xff) //cycleCount+=4;
			pushCycles(4);
		else //cycleCount+=5;
			pushCycles(5);
	}

	public void sta_in(int addr, String reg) {
		if (reg.contains("y")) {
			mem.bytes[mem.peek_word(addr)+cpu.y]=cpu.acu;
		}
		//cycleCount+=6;
		pushCycles(6);
	}

	//non-existent but a good helper
	public void sta_in(int addr, int offset) {
		mem.bytes[mem.peek_word(addr)+offset]=cpu.acu;
		//cycleCount+=6;
		pushCycles(6);
	}
	
	public void stx(int addr) {
		mem.bytes[addr]=cpu.x;
		if (addr<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void ldy_imm(int val) { 
		cpu.y=val;
		if (val==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void ldy(int address) { 
		cpu.y=mem.bytes[address];
		if (cpu.y==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	
	public void ldx_imm(int val) { 
		cpu.x=val;
		if (val==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}

	public void ldx(int address) { 
		cpu.x=mem.bytes[address];
		if (cpu.x==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void lsr() {
		//shift right 1bit, 0->7bit, 0bit->carry
		cpu.acu=cpu.acu>>>1;
		//cycleCount+=2;
		pushCycles(2);
	}

	public void asl() {
		//shift right 1bit, 0->7bit, 0bit->carry
		cpu.acu=cpu.acu<<1;
		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void ora(int address) {
		cpu.acu = cpu.acu | mem.bytes[address];
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}
	
	public void ora_in(int addr, String reg) {
		if (reg.contains("y")) {
			cpu.acu |= mem.bytes[mem.peek_word(addr)+cpu.y];
		}
		if (reg.contains("x")) {
			cpu.acu |= mem.bytes[mem.peek_word(addr+cpu.x)];
		}
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		//cycleCount+=6;
		//cycleCount+=5;
		pushCycles(5);
	}

	
	public void ora_imm(int oramask) {
		cpu.acu = cpu.acu | oramask;
		//cycleCount+=2;
		pushCycles(2);
	}


	public void and(int address) {
		cpu.acu = cpu.acu & mem.bytes[address];
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void and_imm(int andmask) {
		cpu.acu = cpu.acu & andmask;
		//cycleCount+=2;
		pushCycles(2);
	}
	

	public void dec(int address) {
		mem.bytes[address]--;
		if (mem.bytes[address]==0) cpu.zeroFlag=true; else cpu.zeroFlag=false;
		if (mem.bytes[address]<0) mem.bytes[address]=0xff;		
		if (address<0xff) //cycleCount+=5;
			pushCycles(5);
		else //cycleCount+=6;
			pushCycles(6);
	}

	public void inc(int address) {
		mem.bytes[address]++;
		if (mem.bytes[address]>0xff) mem.bytes[address]=0;
		if (mem.bytes[address]==0) cpu.zeroFlag=true; else cpu.zeroFlag=false;			
		if (address<0xff) //cycleCount+=5;
			pushCycles(5);
		else //cycleCount+=6;
			pushCycles(6);
	}
	
	public void isb(int address) {
		mem.bytes[address]=mem.bytes[address]+1;
		cpu.acu=cpu.acu-mem.bytes[address];
		if (cpu.acu==0) cpu.zeroFlag=true;
		else cpu.zeroFlag=false;
		if (address<0xff) //cycleCount+=5;
			pushCycles(5);
		else //cycleCount+=6;
			pushCycles(6);
	}

	public void sbc_imm(int value) {
		if (cpu.acu<value) {
			cpu.acu = 256+cpu.acu-value;
			cpu.negativeFlag=true;
			cpu.carryFlag=false;
		}
		else 
			cpu.acu= cpu.acu-value;
		if (cpu.acu==0) cpu.zeroFlag=true;
		//cycleCount+=2;
		pushCycles(2);
	}

	public void sbc(int address) {

		if (cpu.acu<mem.bytes[address]) {
			cpu.acu= 256+cpu.acu-mem.bytes[address];
			cpu.negativeFlag=true;
			cpu.carryFlag=false;
		}
		else 
			cpu.acu= cpu.acu-mem.bytes[address];
		if (cpu.acu==0) cpu.zeroFlag=true;
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	
	public void pha() {
		cpu.sp--;
		if (cpu.sp<256) cpu.sp=0+255+256;
		mem.bytes[cpu.sp]=cpu.acu;
		//cycleCount+=3;
		pushCycles(3);
	}
	
	public void pla() {
		if (cpu.sp==0+255+256) return; //empty stack
		cpu.acu=mem.bytes[cpu.sp];
		cpu.sp++;
		//cycleCount+=4;
		pushCycles(4);
	}
	
	public void clc() {
		cpu.carryFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}

	public void sec() {
		cpu.carryFlag=true;
		//cycleCount+=2;
		pushCycles(2);
	}

	public void adc_imm(int value) {
		cpu.acu+=value;
		if (cpu.acu>0xff) {
			cpu.acu-=256;
			cpu.carryFlag=true;
		}		
		else cpu.carryFlag=false;
		//cycleCount+=2;
		pushCycles(2);
	}
	
	public void adc(int address) {
		cpu.acu+=mem.bytes[address];
		if (cpu.acu>0xff) {
			cpu.acu-=256;
			cpu.carryFlag=true;
		}
		else cpu.carryFlag=false;
		if (address<0xff) //cycleCount+=3;
			pushCycles(3);
		else //cycleCount+=4;
			pushCycles(4);
	}

	public void adc(int address, String reg) {
		if (reg.contains("x"))
			cpu.acu+=mem.bytes[address+cpu.x];
		if (reg.contains("y"))
			cpu.acu+=mem.bytes[address+cpu.y];		
		if (cpu.acu>0xff) {
			cpu.acu-=256;
			cpu.carryFlag=true;
		}
		else cpu.carryFlag=false;
		pushCycles(4);
	}


	public void jmp(int address) {
		//cycleCount+=3; //jump into
		pushCycles(3);
		runFromMemory(address);
		//cycleCount+=3; //jump back
		pushCycles(3);
	}
	
	public void jmp_in(int address) {
		pushCycles(5);		
		runFromMemory(mem.peek_word(address));
		pushCycles(3);
	}
	
	

	
	
	public void runFromMemory(int address) {
		int opcode = 0xea; //opcode for NOP
		int arg1;
		int arg2;
		while (opcode!=Asm.JMP_ABS && opcode !=0x00 && opcode !=Asm.RTS) {
			// do the code from memory, while JMP absolute is not found, or 00 in memory a hack to make it work
			opcode = mem.bytes[address];
			//if (address<0x1010) System.out.println("JMP:"+String.format("%04X", address)+"->"+getMnemonic(opcode));
			switch (opcode) {
				case Asm.ORA_ABS: //ora $abs
					arg1 = mem.bytes[address+1];
					arg2 = mem.bytes[address+2];
					this.ora(arg2*256+arg1);
					address+=3;
					break;
				case Asm.STA_ZP:
					arg1 = mem.bytes[address+1];
					sta(arg1);
					address+=2;
					break;
				case Asm.LDX_ABS: //ldx $abs
					arg1 = mem.bytes[address+1];
					arg2 = mem.bytes[address+2];
					ldx(arg2*256+arg1);
					address+=3;
					break;
				case Asm.LDA_IMM: // lda imm
					arg1 = mem.bytes[address+1];
					lda_imm(arg1);
					address+=2;
					break;
				case Asm.LDA_ZP: //lda $zp
					arg1 = mem.bytes[address+1];
					lda(arg1);
					address+=2;
					break;
				case Asm.LDA_ZPX: // lda $zp,x
					arg1 = mem.bytes[address+1];
					lda_abs(arg1,"x");
					address+=2;
					break;
				case Asm.STA_ABS: //sta $abs
					arg1 = mem.bytes[address+1];
					arg2 = mem.bytes[address+2];
					sta(arg2*256+arg1);
					address+=3;
					break;
				case Asm.STA_ZPX:
					arg1 = mem.bytes[address+1];
					sta_abs(arg1, "x");
					address+=2;
					break;
				case Asm.INX:
					inx();
					address+=1;
					break;
				case Asm.NOP:
					//do nothing
					address+=1;
					break;
				case Asm.RTS:
					//do nothing
					address+=1;
					break;
				default:
					//fill it with 00 to jump back (to avoid freeze)
					opcode=0x00;
				break;
			}
		}
	}
	
	public void switchBank(boolean additionalMem, int bankNumber) {
		   //portb	equ $D301; // bits: 
		   // 7 - RAM select 1 - on
		   // 6 - not used
             // 5 - ANTIC select (0 - ext, 1 main)
             // 4 - CPU select
             // 3 & 2  bank select
             // 1 - BASIC Enable - 0 - on
             // 0 - OS ROM enable - 0 - off
		   // bank RAM range - $4000 - $7FFF
	           // 3-2: 00 - bank 0, 10 - bank 1, 01 - bank 2, 11 - bank 3
		if (additionalMem) {
			mem.switchBank(bankNumber);
		}
		else {
			//switch back to base RAM
			mem.switchBank(0);			
		}
		this.pushCycles(10); //approximately 10cycles for bank switch (lda portb; do some magic or's/and's; sta portb)
	}

	
	public static int getUpper(int address) {
		return (address & 0b1111111100000000) >>> 8;
	}
	
	public static int getLower(int address) {
		return (address & 0b11111111);
	}
	
	public static String getMnemonic(int opcode) {
		String result="";
		switch(opcode) {
			case Asm.INX: result="inx"; break;
			case Asm.JMP_ABS: result="jmp $abs"; break;
			case Asm.LDA_IMM: result="lda #imm"; break;
			case Asm.LDA_ZP: result="lda $zp"; break;
			case Asm.LDA_ZPX: result="lda $zp,x"; break;
			case Asm.LDX_ABS: result="ldx $abs"; break;
			case Asm.ORA_ABS: result="ora $abs"; break;
			case Asm.STA_ABS: result="sta $abs"; break;
			case Asm.STA_ZP: result="sta $zp"; break;
			case Asm.STA_ZPX: result="sta $zp, x"; break;
			case Asm.NOP: result="nop"; break;
			case Asm.RTS: result="rts"; break;
			default:
				result="opcode not recognized:"+String.format("%02X", opcode);
				break;			
		}
		return result;
		
	}
	
	public static int getMnemonicSize(int opcode) {
		int result=-1;
		switch(opcode) {
			case Asm.INX: result=1; break;
			case Asm.JMP_ABS: result=3; break;
			case Asm.LDA_IMM: result=2; break;
			case Asm.LDA_ZP: result=2; break;
			case Asm.LDA_ZPX: result=2; break;
			case Asm.LDX_ABS: result=3; break;
			case Asm.ORA_ABS: result=3; break;
			case Asm.STA_ABS: result=3; break;
			case Asm.STA_ZP: result=2; break;
			case Asm.STA_ZPX: result=2; break;
			case Asm.NOP: result=1; break;
			case Asm.RTS: result=1; break;
			default:
				result=-1;
				break;			
		}
		return result;		
	}


	public static void asmFailed(String message) {
		System.out.println(message);
		System.exit(0);
	}
		

	public static void asmFailed(String message, int expected, int actual) {
		System.out.print(message);
		System.out.println(" Expected:"+String.format("%02X", expected)+" Actual:"+String.format("%02X", actual));
		System.out.flush();
		System.exit(0);
	}

	public String printHexWord(int address) {
		String result="";
		result="$"+String.format("%02X", mem.bytes[address+1])+String.format("%02X", mem.bytes[address]);
		return result;
	}
	
	public static String printHexValByte(int value) {
		return String.format("%02X", value);
	}
	
	public static String printHexValWord(int value) {
		return String.format("%04X", value);
	}

}

