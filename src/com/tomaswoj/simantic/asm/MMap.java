package com.tomaswoj.simantic.asm;

public class MMap {
	public static final int SAVMSC_L = 0x58; // screen memory pointer  
	public static final int SAVMSC_H = 0x59;
	public static final int SAVMSC = 0x59;
	public static final int RTCLOCK_1=0x12;
	public static final int RTCLOCK_2=0x13;
	public static final int RTCLOCK_3=0x14;
	
	//color registers
	public static final int PCOLR0=0x2c0; //704
	public static final int PCOLR1=0x2c1; //705
	public static final int PCOLR2=0x2c2; //706
	public static final int PCOLR3=0x2c3; //707
	public static final int COLOR0=0x2c4; //708
	public static final int COLOR1=0x2c5;
	public static final int COLOR2=0x2c6;
	public static final int COLOR3=0x2c7;
	public static final int COLOR4=0x2c8; // COLBACK - 712
	public static final int COLBACK=0x2c8; // COLBACK - 712
	public static final int CHBAS=0x2f4; // COLBACK - 712
	
	//others
	public static final int PORTB=0xd301; // eg memory bank switching
}
